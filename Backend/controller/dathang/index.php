<?php
require('modules/dathangClass.php');
$db = new DatHang();
$filename = 'dathang';
$action = '';
$action = $_GET['action'];
switch ($action) {
    case 'dathangline_add':
        require('dathangline_add.php');
        break;
    case 'dathangline_delete':
        require('dathangline_delete.php');
        break;
    case 'list_kt_mshh_dathangline':
        require('list_kt_mshh_dathangline.php');
        break;
    case 'update_dathangline':
        require('update_dathangline.php');
        break;
    case 'add':
        require('add.php');
        break;
    case 'edit':
        require('edit.php');
        break;
    case 'delete':
        require('delete.php');
        break;




    default:
        header('Content-Type: application/json');
        $result = array(
            "code" => 404
        );
        header("HTTP/1.0 404 NOT FOUND");
        echo json_encode($result) . "\n";
        break;
}
