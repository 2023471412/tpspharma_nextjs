<?php
require('modules/giohangClass.php');
$db = new GioHang();
$filename = 'giohang';
$action = '';
$action = $_GET['action'];
switch ($action) {
    case 'listgiohang':
        require('list_giohang.php');
        break;
    case 'cart_delete_all':
        require('cart_delete_all.php');
        break;
    case 'add':
        require('add.php');
        break;
    case 'edit':
        require('edit.php');
        break;
    case 'delete':
        require('delete.php');
        break;
    default:
        header('Content-Type: application/json');
        $result = array(
            "code" => 404
        );
        header("HTTP/1.0 404 NOT FOUND");
        echo json_encode($result) . "\n";
        break;
}
