<?php

class DatHang extends database
{
    public function dathangline_add($msdv, $msdn, $mshh, $tenhh, $dvt, $msnpp, $thuesuat, $ptgiam, $soluong, $gianhap, $giagoc,  $giaban, $thanhtien, $thanhtienvat, $mshhnpp)
    {
        $getall = $this->connect->prepare("INSERT INTO dathangline(rowid_tonkho,lastmodify,msdv, msdn,ngay, mshh, tenhh, dvt, msnpp,thuesuat,ptgiam, soluong, gianhap, giagoc,  giaban, thanhtien, thanhtienvat,time_xacnhan,mshhnpp ) VALUES ((SELECT IFNULL((SELECT rowid FROM tonkho WHERE mshh='$mshh' ORDER BY rowid LIMIT 1), 0)),NOW(),'$msdv', '$msdn',CURRENT_DATE, '$mshh', '$tenhh','$dvt', '$msnpp','$thuesuat', '$ptgiam', '$soluong', (SELECT IFNULL((SELECT gianhap FROM tonkho WHERE mshh='$mshh' ORDER BY rowid LIMIT 1), 0)), '$giagoc', '$giaban', '$thanhtien', '$thanhtienvat',now(),'$mshhnpp')");
        $getall->execute();
    }
    public function dathangline_delete($rowid, $msdn)
    {
        $getall = $this->connect->prepare("DELETE FROM  dathangline WHERE rowid ='$rowid' AND msdn ='$msdn'");
        $getall->execute();
    }
    public function list_kt_mshh_dathangline($msdn, $mshh, $dvt)
    {
        $getall = $this->connect->prepare("SELECT soluong FROM dathangline WHERE msdn='$msdn' and mshh='$mshh' AND trangthaidonhang=0 and soct='' and dvt='$dvt'");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    //Update số lượng trong giỏ hàng
    public function update_dathangline($msdn, $mshh, $soluong, $dvt, $thanhtien, $thanhtienvat)
    {
        $getall = $this->connect->prepare("UPDATE dathangline SET soluong = '$soluong', thanhtien='$thanhtien', thanhtienvat='$thanhtienvat' WHERE msdn='$msdn' AND mshh='$mshh' and dvt='$dvt'");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
    }
}
