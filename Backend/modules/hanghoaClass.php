<?php

class Hanghoa extends database
{
    //todo Sản phẩm giá tốt
    public function list($msnhom, $offset, $limit)
    {
        $msnhom_q = $limit_q = "";
        if ($msnhom) {
            $msnhom_q = " AND a.msnhom = '$msnhom'  order by a.giabanmin ";
        }
        if ($offset) {
            $limit_q = " LIMIT $offset,$limit ";
        }
        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.thuesuat,a.msnhasx,a.msnpp,a.mshhnpp, c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,f.gianhap,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx,
					 ifnull(e.ptgiam ,0)ptgiam , a.tim_start+a.tim as tim
					 from hosohanghoa a 
					 INNER JOIN dmphanloai b ON  a.msnhom = b.msloai 
					 inner join dmphanloai c on a.msnuocsx = c.msloai 
					 inner join dmphanloai d on a.msnhasx = d.msloai  
					 left join ctkm e on a.mshh = e.mshh AND CURRENT_DATE BETWEEN IFNULL( e.tungay,CURRENT_DATE)  AND IFNULL(e.denngay, CURRENT_DATE) AND e.khoa = 0
					 LEFT JOIN tonkho f ON a.mshh = f.mshh
					 WHERE a.trangthai = 1
					 " . $msnhom_q . $limit_q . "");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_all_hanghoa()
    {
        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.thuesuat,a.msnhasx,a.msnpp,a.mshhnpp, c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,f.gianhap,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx,a.sodangky,a.tenhoatchat,
					 ifnull(e.ptgiam ,0)ptgiam , a.tim_start+a.tim as tim
					 from hosohanghoa a 
					 INNER JOIN dmphanloai b ON  a.msnhom = b.msloai 
					 inner join dmphanloai c on a.msnuocsx = c.msloai 
					 inner join dmphanloai d on a.msnhasx = d.msloai  
					 left join ctkm e on a.mshh = e.mshh AND CURRENT_DATE BETWEEN IFNULL( e.tungay,CURRENT_DATE)  AND IFNULL(e.denngay, CURRENT_DATE) AND e.khoa = 0
					 LEFT JOIN tonkho f ON a.mshh = f.mshh
					 WHERE a.trangthai = 1 ");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_hot_items_conlai($soluong, $mshh)
    {
        if ($mshh != "") {
            $order = '';
            $mshhNew = explode(',', $mshh);
            for ($i = 0; $i < count($mshhNew); $i++) {
                if ($mshhNew[$i] != '') {
                    $order .= " AND mshh !='" . $mshhNew[$i] . "'";
                }
            }
        } else {
            $order = '';
        }
        $getall = $this->connect->prepare("SELECT mshh FROM hosohanghoa WHERE trangthai = 1" . $order . " ORDER BY giabanmin asc LIMIT $soluong");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_hot_items($list_conlai)
    {
        if ($list_conlai > 0) {
            $order = '';
            foreach ($list_conlai as $r) {
                $order .= "," . "'" . $r->mshh . "'";
            };
        };
        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.thuesuat,a.msnhasx,a.msnpp,a.mshhnpp, c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,f.gianhap,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx,
					 ifnull(e.ptgiam ,0)ptgiam , a.tim_start+a.tim as tim
					 from hosohanghoa a 
					 INNER JOIN dmphanloai b ON  a.msnhom = b.msloai 
					 inner join dmphanloai c on a.msnuocsx = c.msloai 
					 inner join dmphanloai d on a.msnhasx = d.msloai  
					 left join ctkm e on a.mshh = e.mshh AND CURRENT_DATE BETWEEN IFNULL( e.tungay,CURRENT_DATE)  AND IFNULL(e.denngay, CURRENT_DATE) AND e.khoa = 0
					 LEFT JOIN tonkho f ON a.mshh = f.mshh
					 WHERE a.trangthai = 1 AND a.mshh IN(" . ltrim($order, ',') . ")
					 ");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_sanphamcungnhom($value_msnhom)
    {
        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.thuesuat,a.msnhasx,a.msnpp,a.mshhnpp, c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,f.gianhap,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx,
					 ifnull(e.ptgiam ,0)ptgiam , a.tim_start+a.tim as tim
					 from hosohanghoa a 
					 INNER JOIN dmphanloai b ON  a.msnhom = b.msloai 
					 inner join dmphanloai c on a.msnuocsx = c.msloai 
					 inner join dmphanloai d on a.msnhasx = d.msloai  
					 left join ctkm e on a.mshh = e.mshh AND CURRENT_DATE BETWEEN IFNULL( e.tungay,CURRENT_DATE)  AND IFNULL(e.denngay, CURRENT_DATE) AND e.khoa = 0
					 LEFT JOIN tonkho f ON a.mshh = f.mshh
					 WHERE a.trangthai = 1 AND a.msnhom ='$value_msnhom' LIMIT 10
					 ");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_chitietsp($url)
    {
        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.path_image_child,a.url,a.url_image,a.msnhom,a.thuesuat,a.msnhasx,a.msnpp,a.mshhnpp,a.tenhoatchat,a.quycach,c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,f.gianhap,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx,b.tenloai as tennhom,
					 ifnull(e.ptgiam ,0)ptgiam , a.tim_start+a.tim as tim
					 from hosohanghoa a 
					 INNER JOIN dmphanloai b ON  a.msnhom = b.msloai 
					 inner join dmphanloai c on a.msnuocsx = c.msloai 
					 inner join dmphanloai d on a.msnhasx = d.msloai  
					 left join ctkm e on a.mshh = e.mshh AND CURRENT_DATE BETWEEN IFNULL( e.tungay,CURRENT_DATE)  AND IFNULL(e.denngay, CURRENT_DATE) AND e.khoa = 0
					 LEFT JOIN tonkho f ON a.mshh = f.mshh
					 WHERE a.trangthai = 1 and a.url='$url'");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_motasp($mshh)
    {

        $getall = $this->connect->prepare("SELECT chidinh, chongchidinh, lieudung, tacdungphu, thantrong, tuongtacthuoc, baoquan from motahanghoa  where mshh='$mshh'");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_hanghoa_theonhom($value_msnhom)
    {

        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.msnhasx,a.msnpp,c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,, a.tim_start+a.tim as tim
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx from hosohanghoa a INNER JOIN dmphanloai b ON  a.msnhom = b.msloai inner join dmphanloai c on a.msnuocsx = c.msloai inner join dmphanloai d on a.msnhasx = d.msloai   WHERE b.dieukien2 = '$value_msnhom' and a.trangthai = 1 ");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_toanbo_hanghoa_theonhom($value_msnhom)
    {

        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.msnhasx,a.msnpp,c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,a.tim_start+a.tim as tim,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx from hosohanghoa a INNER JOIN dmphanloai b ON  a.msnhom = b.msloai inner join dmphanloai c on a.msnuocsx = c.msloai inner join dmphanloai d on a.msnhasx = d.msloai   WHERE b.dieukien2 = '$value_msnhom' and a.trangthai = 1 order by a.giabanmin");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_search($value)
    {
        $getall = $this->connect->prepare("SELECT a.mshh, a.tenhh, b.tenloai, a.giabanmin, a.giabanmax,a.path_image,a.url, a.dvtmin, a.dvtmax, a.quycach from hosohanghoa a INNER JOIN dmphanloai b ON a.msnhom = b.msloai  where a.trangthai = 1 and tenhh LIKE '%$value%' or tenhoatchat LIKE '%$value%' limit 0,10 ");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    public function list_filter($data)
    {
        $msnhasx = " ";
        $tieuchuan = " ";
        $nuoc = " ";
        if (count($data['msnhasx']) > 0) {
            $msnhasx_arr = implode(",", $data['msnhasx']);
            $msnhasx = " AND a.msnhasx IN($msnhasx_arr) ";
        }
        if (count($data['tieuchuan']) > 0) {
            $tieuchuan_arr = implode(",", $data['tieuchuan']);
            $tieuchuan = " AND a.tieuchuan IN($tieuchuan_arr) ";
        }
        if (count($data['nuoc']) > 0) {
            $nuoc_arr = implode(",", $data['nuoc']);
            $nuoc = " AND a.msnuocsx IN($nuoc_arr) ";
        }
        if (count($data['msnhom']) > 0) {
            $nhom_arr = implode(",", $data['msnhom']);
            $nhom = " AND a.msnhom IN($nhom_arr) ";
        }
        $getall = $this->connect->prepare("SELECT a.mshh,a.tenhh,a.path_image,a.url,a.url_image,a.msnhom,a.thuesuat,a.msnhasx,a.msnpp,a.mshhnpp, c.tenloai as msnuocsx,a.dvtmax,a.slquydoi,a.tieuchuan,a.giabanmax,a.dvtmin,
        if(a.bantheodon='1',' | Bán theo đơn','') as theodon,f.gianhap,
                a.giabanmin,a.hamluong,(a.giabanmin*a.ptgiagoc)/100+a.giabanmin as giabangoc ,d.tenloai as tennhasx,a.tim_start+a.tim as tim,
					 ifnull(e.ptgiam ,0)ptgiam 
					 from hosohanghoa a 
					 INNER JOIN dmphanloai b ON  a.msnhom = b.msloai 
					 inner join dmphanloai c on a.msnuocsx = c.msloai 
					 inner join dmphanloai d on a.msnhasx = d.msloai  
					 left join ctkm e on a.mshh = e.mshh AND CURRENT_DATE BETWEEN IFNULL( e.tungay,CURRENT_DATE)  AND IFNULL(e.denngay, CURRENT_DATE) AND e.khoa = 0
					 LEFT JOIN tonkho f ON a.mshh = f.mshh
					 WHERE a.trangthai = 1  " . $msnhasx . $tieuchuan . $nuoc . $nhom . "");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    //todo post lượt xen sản phẩm
    public function post_luotxem($msdn, $ipthietbi, $thietbi, $mshh, $tim)
    {
        $getall = $this->connect->prepare("INSERT INTO luotxemsanpham(lastmodify,msdn, ipthietbi, thietbi,mshh, tim)
       VALUES (NOW(),'$msdn','$ipthietbi','$thietbi','$mshh','$tim')");
        $getall->execute();
    }
    //todo get tim
    public function get_tim($mshh)
    {
        $getall = $this->connect->prepare("SELECT tim, tim_start+tim as tim_all from hosohanghoa where mshh='$mshh'");
        $getall->setFetchMode(PDO::FETCH_OBJ);
        $getall->execute();
        return $getall->fetchAll();
    }
    //todo update tim
    public function update_tim($mshh, $timnew)
    {
        $getall = $this->connect->prepare("UPDATE hosohanghoa set tim = '$timnew' WHERE mshh='$mshh'");
        $getall->execute();
    }
}
