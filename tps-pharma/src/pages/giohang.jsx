import React, { useState, useEffect } from "react";
import GioHangItem from "../components/giohang/giohang_item";
import giohangApi from "./api/giohangApi";
import thanhtoanApi from "./api/thanhtoanApi";
import { useSelector, useDispatch } from "react-redux";
import { giohangState$ } from "../components/redux/selectors";
import { createGiohang } from "../components/redux/actions";
import Image from "next/image";
import { useRouter } from "next/router";

const Cart = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  // const navigate = useNavigate();
  const [showModal, setShowModal] = useState(false);
  const [showModal_Delete_All, setShowModal_Delete_All] = useState(false);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [giohang, setGiohangItem] = useState([]);
  const giohangthanhtien = useSelector(giohangState$);
  const [isCheck, setIsCheck] = useState([]);
  const [tongtien, setTongTien] = useState();
  const dispatch = useDispatch();
  const [localStorage, setMsdn] = useState();
  const [localStorage_Tendv, setTendv] = useState();
  const [localStorage_Diachi, setDiachi] = useState();
  const [localStorage_Sodienthoai, setSodienthoai] = useState();
  const [localStorage_Tendaidien, setTendaidien] = useState();
  const router = useRouter();
  useEffect(() => {
    setMsdn(window.localStorage.getItem("msdn"));
    setTendv(window.localStorage.getItem("tendv"));
    setTendaidien(window.localStorage.getItem("tendaidien"));
    setDiachi(window.localStorage.getItem("diachi"));
    setSodienthoai(window.localStorage.getItem("sodienthoai"));
  }, [localStorage]);
  //todo Tính tổng tiền
  useEffect(() => {
    const loadTongtien = () => {
      const kt_check = document.getElementById("cart_item_checkall");

      const sum = giohangthanhtien.reduce((thanhtien, item) => {
        for (let i = 0; i < isCheck.length; i++) {
          if (isCheck[i] === item.rowid) {
            return parseInt(thanhtien) + parseInt(item.thanhtien);
          }
        }
        if (kt_check.checked) {
          return parseInt(thanhtien) + parseInt(item.thanhtien);
        }
        return parseInt(thanhtien);
      }, 0);
      setTongTien(sum?.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    };
    loadTongtien();
  }, [isCheck, giohangthanhtien]);

  //todo Load giỏ hàng item
  useEffect(() => {
    if (localStorage) {
      const handleGiohang = async (e) => {
        try {
          const params = {
            msdn: localStorage,
          };
          const response = await giohangApi.listgiohang(params);
          setGiohangItem(response);
        } catch (error) {
          console.log(error);
        }
      };
      handleGiohang();
    }
  }, [giohangthanhtien, localStorage]);

  //todo Check tất cả item trong giỏ hàng
  const handleClickAll = (e) => {
    const kt_check = document.getElementById("cart_item_checkall");
    const a = document.getElementsByClassName("cart_item_check");
    if (kt_check.checked) {
      for (let i = 0; i < a.length; i++) {
        a[i].checked = true;
      }
      setIsCheckAll(true);
      const id = giohang.map((item) => item.rowid);
      setIsCheck(id);
    } else {
      setIsCheck([]);
      for (let e = 0; e < a.length; e++) {
        a[e].checked = false;
      }
    }
  };
  useEffect(() => {
    setTimeout(() => {
      const handleClickAll = (e) => {
        const kt_check = document.getElementById("cart_item_checkall");
        const a = document.getElementsByClassName("cart_item_check");
        if (kt_check.checked) {
          for (let i = 0; i < a.length; i++) {
            a[i].checked = true;
          }
          setIsCheckAll(true);
          const id = giohangthanhtien.map((item) => item.rowid);
          setIsCheck(id);
        } else {
          setIsCheck([]);
          for (let e = 0; e < a.length; e++) {
            a[e].checked = false;
          }
        }
      };
      handleClickAll();
    }, 500);
  }, []);

  //todo check từng item trong giỏ hàng
  const handleClick = (e) => {
    const { value, checked } = e.target;
    const kt_check = document.getElementById("cart_item_checkall");
    kt_check.checked = false;
    setIsCheck([...isCheck, value]);
    if (!checked) {
      setIsCheck(isCheck.filter((item) => item !== value));
    }
  };
  //todo Thanh toán
  const handleThanhtoan = (e) => {
    if (isCheck == "") {
      //*chưa có check sp nào cả
      setShowModal(true);
    } else {
      //*thỏa điều kiện thì cho mua hàng
      const soct = "ID" + Date.now() + Math.floor(1000 + Math.random() * 9000);
      const thanhtienvat = tongtien.replaceAll(".", "");

      const handleDathangHeader_add = async (e) => {
        try {
          const params = {
            msdv: "",
            msdn: localStorage,
            mskh: "",
            tenkhachhang: localStorage_Tendv,
            tendaidien: localStorage_Tendaidien,
            dienthoai: localStorage_Sodienthoai,
            diachi: localStorage_Diachi,
            soct: soct,
            thanhtienvat: thanhtienvat,
          };
          console.log(params);
          await thanhtoanApi.dathangheader_add(params);
          handleGiohang();
        } catch (error) {
          console.log(error);
        }
      };
      handleDathangHeader_add();

      for (let i = 0; i < isCheck.length; i++) {
        const handleUpdateLine = async (e) => {
          try {
            const params = {
              msdn: localStorage,
              soct: soct,
              rowid: isCheck[i],
            };
            await thanhtoanApi.update_line_1(params);
          } catch (error) {
            console.log(error);
          }
        };
        handleUpdateLine();
      }
      router.push("/thanhtoan?" + soct);
    }
  };
  //todo delete all cart
  function handleGiohang(e) {
    const params = {
      msdn: localStorage,
    };
    dispatch(createGiohang.createGiohangRequest(params));
  }
  function cart_delete_all() {
    var msdn = localStorage;
    const handleGiohangDeleteAll = async (e) => {
      try {
        const params = {
          msdn: msdn,
        };
        await giohangApi.cart_delete_all(params);
        handleGiohang();
        setShowModal_Delete_All(false);
      } catch (error) {
        console.log(error);
      }
    };
    handleGiohangDeleteAll();
  }
  return (
    <>
      <div className="bg-[#eeebeb] lglap:px-5 smlap:px-10">
        <div className="fullscreen:container mx-auto">
          <div className="grid grid-cols-12  py-3 gap-7 mobile:gap-2 mobile:px-2 tablet:gap-2 tablet:px-3">
            <div className="col-span-8 bg-white p-5 rounded-md tablet:col-span-12 mobile:p-2  mobile:col-span-12">
              <div className="flex justify-between">
                <p className="text-sm font-semibold text-[red]">Giỏ hàng</p>
                <p
                  className="text-[green] hover:cursor-pointer"
                  onClick={(e) => setShowModal_Delete_All(true)}
                >
                  Xóa tất cả
                </p>
              </div>
              {/* Chi tiết giỏ hàng */}
              <div className="flex flex-col ">
                <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full">
                        <thead className="border-b">
                          <tr className="relative">
                            <th>
                              <input
                                className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-center mr-2 cursor-pointer"
                                type="checkbox"
                                value=""
                                id="cart_item_checkall"
                                onClick={handleClickAll}
                                defaultChecked
                              />
                            </th>
                            <th
                              scope="col"
                              className="header_tb_giohang text-center mobile:whitespace-nowrap"
                            >
                              Sản phẩm
                            </th>
                            <th scope="col" className="header_tb_giohang mobile:hidden">
                              Đơn giá
                            </th>
                            <th scope="col" className="header_tb_giohang mobile:absolute mobile:right-5">
                              Số lượng
                            </th>
                            <th
                              scope="col"
                              className="header_tb_giohang whitespace-nowrap  mobile:hidden"
                            >
                              Thành tiền
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {giohang.map((giohangItem, key) => {
                            return (
                              <GioHangItem
                                giohangItem={giohangItem}
                                index={key}
                                handleClick={handleClick}
                                isChecked={isCheck.includes(key)}
                              />
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Thông tin khách hàng lấy api của A.Vi */}
            <div className="col-span-4 tablet:col-span-12 mobile:col-span-12">
              <div className=" bg-white p-5 rounded-md max-h-52">
                <div className="  text-xl ">
                  <div className="text-sm font-semibold text-[red]">
                    Khách hàng
                  </div>
                  <div className="text-[14px] pl-[30px] mobile:pl-2">
                    <div className="py-1 mobile:py-2 flex items-center gap-2 ">
                      <div>
                        <Image
                          src={require("../pages/assets/img/icon/store.png")}
                        />
                      </div>
                      <span>{localStorage_Tendv}</span>
                    </div>
                    <div className="py-1 mobile:py-2  flex items-center gap-2">
                      <div>
                        <Image
                          src={require("../pages/assets/img/icon/user.png")}
                        />
                      </div>
                      <span>{localStorage_Tendaidien}</span>
                    </div>
                    <div className="py-1 mobile:py-2 flex items-center gap-2">
                      <div>
                        <Image
                          src={require("../pages/assets/img/icon/phone.png")}
                        />
                      </div>
                      <span>{localStorage_Sodienthoai}</span>
                    </div>
                    <div className="py-1 mobile:py-2 flex items-center gap-2">
                      <div>
                        <Image
                          src={require("../pages/assets/img/icon/address.png")}
                        />
                      </div>
                      <span>{localStorage_Diachi}</span>
                    </div>
                  </div>
                </div>
                <div className="flex justify-around pt-10"></div>
              </div>
              <div className=" bg-white p-5 mobile:mt-2 tablet:mt-[8px] mt-5 rounded-md max-h-52">
                <div className="flex justify-between  font-semibold">
                  <div className="text-[red] text-sm font-semibold">
                    Tổng tiền
                  </div>
                  <div className="flex items-end">
                    <p className="text-sm text-[red] font-semibold">
                      {tongtien} đ
                    </p>
                  </div>
                </div>
                <div className="flex justify-around pt-10">
                  <button
                    type="submit"
                    onClick={handleThanhtoan}
                    className="bg-[green] w-full text-center text-white p-3 rounded-lg"
                  >
                    Đặt hàng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Modal */}
      {showModal ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative w-auto pointer-events-none">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-body relative p-4">
                <div className="px-6 text-center">
                  <svg
                    aria-hidden="true"
                    className="mx-auto mb-4 text-gray-400 w-14 h-14 dark:text-gray-200"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                    ></path>
                  </svg>
                  <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                    Bạn chưa chọn sản phẩm nào
                  </h3>
                  <button
                    onClick={() => setShowModal(false)}
                    data-modal-hide="popup-modal"
                    type="button"
                    className="text-white bg-green-500 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2"
                  >
                    Đồng ý
                  </button>
                  <button
                    onClick={() => setShowModal(false)}
                    data-modal-hide="popup-modal"
                    type="button"
                    className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      )}
      {/* Modal delete all item */}
      {showModal_Delete_All ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative w-auto pointer-events-none">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-body relative p-4">
                <div className="px-6 text-center">
                  <svg
                    aria-hidden="true"
                    className="mx-auto mb-4 text-red-500 w-14 h-14 dark:text-gray-200"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                    ></path>
                  </svg>
                  <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                    Bạn có chắc xóa tất cả?
                  </h3>
                  <button
                    onClick={() => cart_delete_all()}
                    data-modal-hide="popup-modal"
                    type="button"
                    className="text-white bg-green-500 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2"
                  >
                    Đồng ý
                  </button>
                  <button
                    onClick={() => setShowModal_Delete_All(false)}
                    data-modal-hide="popup-modal"
                    type="button"
                    className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};
export default Cart;
