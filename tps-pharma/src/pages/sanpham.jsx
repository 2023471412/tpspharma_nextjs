import React, { useState, useEffect } from "react";
import Image from "next/image";
import hanghoaApi from "./api/hanghoaApi";
import dathangApi from "./api/dathangApi";
import MoTaSanPham from "../components/sanpham/mota_sanpham";
import SanphamItem from "../components/sanpham/sanpham_item";
import { useDispatch } from "react-redux";
import { createGiohang, showModal, showModalImage } from "../components/redux/actions";
import { useRouter } from "next/router";
import ModalImg from "@/components/sanpham/modal_img";

function sanpham() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const dispatch = useDispatch();
  // let { url_sanpham } = useParams();
  const [ctHanghoa, setctHanghoa] = useState([]);
  const [soluong, setSoluong] = useState(1);
  const [sanphamcungnhom, setSanPhamCungNhom] = useState([]);
  const router = useRouter();
  const key = router.query;
  const url_sanpham = Object.keys(key)[0];
  //get chi tiết hàng hóa
  const [localStorage, setStatus] = useState();

  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
  }, []);
  useEffect(() => {
    const handleHanghoa = async (e) => {
      try {
        const params = {
          url: url_sanpham,
        };
        const response = await hanghoaApi.listchitietsp(params);
        setctHanghoa(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleHanghoa();
  }, [url_sanpham]);
  function soluong_tang() {
    const soluong_element = document.getElementsByClassName("input_sl")[0];
    var soluong = soluong_element.value;
    var soluong_new = Number(soluong) + 1;
    soluong_element.value = soluong_new;
  }
  function soluong_giam() {
    const soluong_element = document.getElementsByClassName("input_sl")[0];
    var soluong = soluong_element.value;
    if (soluong <= 1) {
    } else {
      var soluong_new = Number(soluong) - 1;
      soluong_element.value = soluong_new;
    }
  }

  function handleGiohang(e) {
    const params = {
      msdn: localStorage,
    };
    dispatch(createGiohang.createGiohangRequest(params));
  }

  function dathangline_add(index, e) {
    var ten_sanpham = e.tenhh,
      mshh = e.mshh,
      msnpp = e.msnpp,
      mshhnpp = e.mshhnpp,
      dvtmin = e.dvtmin,
      dvtmax = e.dvtmax,
      gianhap = e.gianhap,
      ptgiam = e.ptgiam,
      giagocmin = e.giabanmin,
      giagocmax = e.giabanmax,
      giabanmin = giagocmin - (giagocmin * ptgiam) / 100,
      giabanmax = giagocmax - (giagocmax * ptgiam) / 100,
      thuesuat = e.thuesuat;
    var soluong = document.getElementsByClassName("input_sl")[index].value;
    var thanhtienmin = giabanmin * soluong;
    var thanhtienmax = giabanmax * soluong;
    var thanhtienvatmin = thanhtienmin - (thanhtienmin * thuesuat) / 100;
    var thanhtienvatmax = thanhtienmax - (thanhtienmax * thuesuat) / 100;
    var checked_giabanmin =
      document.getElementsByClassName("checked_giabanmin")[index];
    var checked_giabanmax =
      document.getElementsByClassName("checked_giabanmax")[index];
    if (localStorage) {
      // const [ktmshh, setKtmshh] = React.useState([]);
      const token = localStorage;
      if (token == "block") {
        dispatch(showModal());
      } else if (token == undefined) {
        dispatch(showModal());
      } else {
        if (checked_giabanmin.checked == true) {
          //Xử lý khi sản phẩm có trong giỏ hàng min
          const handleKTGH = async (e) => {
            try {
              const params = {
                msdn: localStorage,
                mshh: mshh,
                dvt: dvtmin,
              };
              const response = await dathangApi.list_kt_mshh_dathangline(
                params
              );
              if (response.length == 0) {
                const handleDathang = async (e) => {
                  try {
                    const params = {
                      msdv: "",
                      msdn: localStorage,
                      mshh: mshh,
                      tenhh: ten_sanpham,
                      dvt: dvtmin,
                      mshhnpp: mshhnpp,
                      msnpp: msnpp,
                      ptgiam: ptgiam,
                      thuesuat: thuesuat,
                      soluong: soluong,
                      gianhap: gianhap,
                      giagoc: giagocmin,
                      giaban: giabanmin,
                      thanhtien: thanhtienmin,
                      thanhtienvat: thanhtienvatmin,
                    };
                    await dathangApi.dathangline_add(params);
                    handleGiohang();
                  } catch (error) {
                    console.log(error);
                  }
                };
                handleDathang();
              } else {
                const handleUpdate_dathangline = async (e) => {
                  const soluongNew =
                    parseInt(response[0].soluong) + parseInt(soluong);
                  const thanhtienNew = giabanmin * parseInt(soluongNew);
                  const thanhtienvatNew =
                    thanhtienNew - (thanhtienNew * thuesuat) / 100;
                  try {
                    const params = {
                      msdn: localStorage,
                      mshh: mshh,
                      soluong: soluongNew,
                      dvt: dvtmin,
                      thanhtien: thanhtienNew,
                      thanhtienvat: thanhtienvatNew,
                    };
                    await dathangApi.update_dathangline(params);
                    handleGiohang();
                  } catch (error) {
                    console.log(error);
                  }
                };
                handleUpdate_dathangline();
              }
            } catch (error) {
              console.log(error);
            }
          };
          handleKTGH();
        } else if (checked_giabanmax.checked == true) {
          //Xử lý khi sản phẩm có trong giỏ hàng max

          const handleKTGH = async (e) => {
            try {
              const params = {
                msdn: localStorage,
                mshh: mshh,
                dvt: dvtmax,
              };
              const response = await dathangApi.list_kt_mshh_dathangline(
                params
              );
              if (response.length == 0) {
                const handleDathang = async (e) => {
                  try {
                    const params = {
                      msdv: "",
                      msdn: localStorage,
                      mshh: mshh,
                      tenhh: ten_sanpham,
                      dvt: dvtmax,
                      msnpp: msnpp,
                      mshhnpp: mshhnpp,
                      ptgiam: ptgiam,
                      thuesuat: thuesuat,
                      soluong: soluong,
                      gianhap: gianhap,
                      giagoc: giagocmax,
                      giaban: giabanmax,
                      thanhtien: thanhtienmax,
                      thanhtienvat: thanhtienvatmax,
                    };
                    await dathangApi.dathangline_add(params);
                    handleGiohang();
                  } catch (error) {
                    console.log(error);
                  }
                };
                handleDathang();
              } else {
                const handleUpdate_dathangline = async (e) => {
                  const soluongNew =
                    parseInt(response[0].soluong) + parseInt(soluong);
                  const thanhtienNew = giabanmax * parseInt(soluongNew);
                  const thanhtienvatNew =
                    thanhtienNew - (thanhtienNew * thuesuat) / 100;
                  try {
                    const params = {
                      msdn: localStorage,
                      mshh: mshh,
                      soluong: soluongNew,
                      dvt: dvtmax,
                      thanhtien: thanhtienNew,
                      thanhtienvat: thanhtienvatNew,
                    };
                    await dathangApi.update_dathangline(params);
                    handleGiohang();
                  } catch (error) {
                    console.log(error);
                  }
                };
                handleUpdate_dathangline();
              }
            } catch (error) {
              console.log(error);
            }
          };
          handleKTGH();

          //
        } else {
          // setOpenWarning(true);
        }
      }
    } else {
      dispatch(showModal());
    }
  }
  useEffect(() => {
    const sanphamcungnhomHandle = async (e) => {
      try {
        const params = {
          value_msnhom: ctHanghoa[0].msnhom,
        };
        const response = await hanghoaApi.list_sanphamcungnhom(params);
        setSanPhamCungNhom(response);
      } catch (error) {
        // console.log(error);
      }
    };
    sanphamcungnhomHandle();
  }, [ctHanghoa]);
  return (
    <>
      <div className="bg-[#eeebeb] lglap:px-5">
        <div className="bg-white ">
          <div className="fullscreen:container mx-auto lglap:px-5 smlap:px-10">
            <div className="grid grid-cols-12 py-10 gap-10 mobile:gap-0 mobile:py-5 mobile:px-2 tablet:px-3">
              {ctHanghoa.map((cthanghoa_item, key) => {
                const img_childs = cthanghoa_item.path_image_child;
                return (
                  <>
                    <div className="col-span-5 mobile:col-span-12  grid grid-cols-12">
                      <div className="border rounded-xl flex justify-center col-span-12">
                        <div className="">
                          <Image
                            onClick={(e) => {
                              dispatch(showModalImage());
                            }}
                            className="rounded-xl"
                            src={require(`../../../Backend/upload/sanpham/${cthanghoa_item.path_image}`)}
                            alt={cthanghoa_item.path_image}
                          />
                        </div>
                      </div>
                      <div className="img_mota col-span-12 pt-3">
                        <div className="grid grid-cols-12 gap-3">
                          {img_childs
                            ? img_childs.split("|").map((img_child, key) => {
                                return (
                                  <div className="col-span-3">
                                    <div className="">
                                      <Image
                                        onClick={(e) => {
                                          dispatch(showModalImage());
                                        }}
                                        className="rounded-sm "
                                        src={require(`../../../Backend/upload/anhmota/${img_child}`)}
                                        alt={img_child}
                                      />
                                    </div>
                                  </div>
                                );
                              })
                            : ""}
                        </div>
                      </div>
                    </div>
                    <div className="col-span-7 mobile:col-span-12">
                      <div>
                        <h1 className="font-bold text-2xl text-[green] mobile:text-xl">
                          {cthanghoa_item.tenhh}
                        </h1>
                      </div>
                      <div className="grid grid-cols-12 gap-5 pt-5">
                        <div className="col-span-7 tablet:col-span-12 mobile:col-span-12">
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Nhóm</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.tennhom}</p>
                            </div>
                          </div>
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Hoạt chất</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.tenhoatchat}</p>
                            </div>
                          </div>
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Hàm lượng</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.hamluong}</p>
                            </div>
                          </div>
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Quy cách</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.quycach}</p>
                            </div>
                          </div>
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Tiêu chuẩn</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.tieuchuan}</p>
                            </div>
                          </div>
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Nhà sản xuất</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.tennhasx}</p>
                            </div>
                          </div>
                          <div className="mb-3 grid grid-cols-12 ">
                            <div className="col-span-4">
                              <p>• Nước sản xuất</p>
                            </div>
                            <div className="col-span-8 ">
                              <p>{cthanghoa_item.msnuocsx}</p>
                            </div>
                          </div>

                          <div className="grid grid-cols-12 gap-5 items-end mobile:px-2">
                            <div className="col-span-5">
                              <div className="col-span-2 pt-5 flex items-right justify-start">
                                <input
                                  type="radio"
                                  className=" mr-2 checked_giabanmin"
                                  name={"giaban_" + cthanghoa_item.mshh}
                                  defaultChecked
                                />
                                <p className="text-[red]  text-2xl">
                                  {cthanghoa_item.giabanmin
                                    .toString()
                                    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                                  đ
                                </p>
                                <p className="text-2xl text-[red] pr-2">
                                  /{cthanghoa_item.dvtmin}
                                </p>
                              </div>
                              <div className="flex col-span-2 items-center justify-start mb-5 mt-2">
                                <input
                                  type="radio"
                                  className="mr-2 checked_giabanmax"
                                  name={"giaban_" + cthanghoa_item.mshh}
                                />
                                <p className="text-[red]  text-2xl">
                                  {cthanghoa_item.giabanmax
                                    .toString()
                                    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                                  đ
                                </p>
                                <p className="text-2xl text-[red] pr-2">
                                  /{cthanghoa_item.dvtmax}
                                </p>
                              </div>
                              <div className="flex items-center justify-between w-full p-1 border-2 border-gray-400 rounded-lg">
                                <button onClick={soluong_giam} className="">
                                  <Image
                                    src={require("./assets/img/icon/minus.png")}
                                    alt="minus"
                                  ></Image>
                                </button>
                                <input
                                  onChange={(e) =>
                                    setSoluong(
                                      e.target.value.replace(/[^0-9\.\,]/g, "")
                                    )
                                  }
                                  className="input_sl w-10 text-lg text-center outline-none"
                                  value={soluong}
                                  readOnly
                                ></input>
                                <button onClick={soluong_tang} className="">
                                  <Image
                                    src={require("./assets/img/icon/plus.png")}
                                    alt="plus"
                                  ></Image>
                                </button>
                              </div>
                            </div>
                            <div className="col-span-7">
                              <button
                                onClick={(e) =>
                                  dathangline_add(key, cthanghoa_item)
                                }
                                className=" bg-[green] w-[60%] mobile:w-full text-white rounded-lg px-3 ml-3 py-2 mobile:ml-0"
                              >
                                Thêm vào giỏ
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                );
              })}
            </div>
          </div>
        </div>

        <div className="fullscreen:container mx-auto py-5 tablet:px-5 mobile:px-2 smlap:px-10">
          <div className="bg-white rounded-xl">
            {ctHanghoa.map((cthanghoa_item, key) => {
              return <MoTaSanPham mota_sanpham={cthanghoa_item} />;
            })}
          </div>
          <div className="bg-white rounded-xl mt-5">
            <div className="p-5 mobile:p-2">
              <div className="font-bold text-[black]">Sản phẩm cùng nhóm</div>
              <div className="pt-5">
                <div className="grid grid-cols-5 gap-3 pt-5 tablet:grid-cols-3 mobile:grid-cols-2">
                  {sanphamcungnhom?.map((sanphamcungnhom_item, key) => {
                    return (
                      <SanphamItem
                        hanghoa_item={sanphamcungnhom_item}
                        index={key}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ModalImg url_sanpham={url_sanpham}/>
    </>
  );
}

export default sanpham;
