import React, { useState, useEffect } from "react";

import { useRouter } from "next/router";

const Footer_item = () => {
  const router = useRouter();
  const key = router.query;
  const loaifooter = Object.keys(key)[0];
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      {(() => {
        switch (loaifooter) {
          case "gioi-thieu":
            return (
              <div id="gioithieu" className="gioithieu">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Giới thiệu</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          case "pham-vi-cung-cap":
            return (
              <div id="phamvicungcap" className="phamvicungcap">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Phạm vi cung cấp</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          case "chinh-sach-doi-tra":
            return (
              <div id="doitra" className="doitra">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Chính sách đổi trả</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          case "chinh-sach-giao-hang":
            return (
              <div id="giaohang" className="giaohang">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Chính sách giao hàng</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          case "chinh-sach-bao-mat":
            return (
              <div id="baomat" className="baomat">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Chính sách bảo mật</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          case "chinh-sach-thanh-toan":
            return (
              <div id="thanhtoan" className="thanhtoan">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Chính sách thanh toán</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          case "chinh-sach-thanh-vien":
            return (
              <div id="thanhvien" className="thanhvien">
                <div className="bg_body">
                  <div className="container mx-auto py-5 mobile:py-2 tablet:py-3">
                    <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham mobile:px-2 tablet:grid-cols-4 tablet:px-3 tablet:pt-2">
                      <p>Chính sách thành viên</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          default:
            break;
        }
      })()}
    </>
  );
};

export default Footer_item;
