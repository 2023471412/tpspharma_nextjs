import axiosClient_not_token from "./axiosClient_not_token";

const giohangApi = {
  listgiohang: (params) => {
    const url = "/giohang/listgiohang";
    return axiosClient_not_token.post(url, params);
  },
  cart_delete_all: (params) => {
    const url = "/giohang/cart_delete_all";
    return axiosClient_not_token.post(url, params);
  },
};

export default giohangApi;
