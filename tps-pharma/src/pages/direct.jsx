import React, { useState, useEffect } from "react";
import userApi from "./api/userApi";
import { useDispatch } from "react-redux";
import { createGiohang } from "../components/redux/actions";
import { useRouter } from "next/router";

function Direct() {
  const router = useRouter()
  const  token  = router.asPath.split('?')[1];
  const [openModal, setOpenModal] = useState(false);
  const dispatch = useDispatch();
  const [localStorage, setStatus] = useState();

  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
  }, []);
  function handleGiohang(e) {
    const params = {
      msdn: localStorage,
    };
    dispatch(createGiohang.createGiohangRequest(params));
  }
  const handleAutoLogin = async (e) => {
    if (token == "405") {
      //token của useparam; 404 của useparam
      window.localStorage.setItem("msdn", "block");
      router.push("/");
      //out khỏi
    } else {
      const params = {
        token: token,
      };
      const response = await userApi.login_direct(params);

      window.localStorage.setItem("token", response.token);
      window.localStorage.setItem("msdv", response.msdv);
      window.localStorage.setItem("tendv", response.tendv);
      window.localStorage.setItem("msdn", response.msdn);
      window.localStorage.setItem("tendaidien", response.tendaidien);
      window.localStorage.setItem("diachi", response.diachi);
      window.localStorage.setItem("dienthoai", response.dienthoai);
      router.push("/");
      handleGiohang();
    }
  };
  handleAutoLogin();
  return (
    <>
      {openModal ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative w-auto pointer-events-none">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-header flex flex-shrink-0 items-center justify-between p-4 border-b border-gray-200 rounded-t-md">
                <h5
                  className="text-xl font-medium leading-normal text-red-600"
                  id="exampleModalLabel"
                >
                  Tài khoản hiện không có
                </h5>
              </div>
              <div className="modal-body relative p-4">
                <div className="text-center lg:text-left flex justify-center">
                  <button
                    onClick={(e) => router.push("/")}
                    type="button"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    className="close_login"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

export default Direct;
