import { useEffect } from "react";
import "./assets/css/index.css";
import Layout from "./Layout";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSageMiddleware from "redux-saga";
import reducers from "../components/redux/reducers";
import mySaga from "../components/redux/sagas";
import { composeWithDevTools } from "redux-devtools-extension";
import { useRouter } from "next/router";
const sagaMiddleware = createSageMiddleware();
const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);
sagaMiddleware.run(mySaga);
export default function App({ Component, pageProps }) {
  const router = useRouter();
  useEffect(() => {
    const use = async () => {
      (await import("tw-elements")).default;
    };
    use();
  }, []);
  return (
    <Provider store={store}>
      <Layout>
        <Component  {...pageProps} />
      </Layout>
    </Provider>
  );
}
