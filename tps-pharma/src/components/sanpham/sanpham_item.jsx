import React, { useState, useEffect } from "react";
import dathangApi from "../../pages/api/dathangApi";
import hanghoaApi from "../../pages/api/hanghoaApi";
import Link from "next/link";

import { useDispatch } from "react-redux";
import { createGiohang, showModal } from "../redux/actions";
import Image from "next/image";
import { useRouter } from "next/router";
const SanphamItem = (prop) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const hanghoa_item = prop.hanghoa_item;
  const danhmuc_item = prop.danhmuc_item;
  const index = prop.index;
  const [soluong, setSoluong] = useState(1);
  const [tim, setTim] = useState(hanghoa_item.tim);
  const [localStorage, setStatus] = useState();

  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
  }, []);
  function soluong_tang(index, e) {
    const a = e.currentTarget.parentNode.getElementsByClassName("input_sl")[0];
    var soluong = a.value;
    var soluong_new = Number(soluong) + 1;
    a.value = soluong_new;
  }
  function soluong_giam(key, e) {
    const a = e.currentTarget.parentNode.getElementsByClassName("input_sl")[0];
    var soluong = a.value;
    if (soluong <= 1) {
    } else {
      var soluong_new = Number(soluong) - 1;
      a.value = soluong_new;
    }
  }
  function handleGiohang(e) {
    const params = {
      msdn: localStorage,
    };
    dispatch(createGiohang.createGiohangRequest(params));
  }

  function dathangline_add(index, e, is) {
    var ten_sanpham = e.tenhh,
      mshh = e.mshh,
      msnpp = e.msnpp,
      mshhnpp = e.mshhnpp,
      dvtmin = e.dvtmin,
      dvtmax = e.dvtmax,
      gianhap = e.gianhap,
      ptgiam = e.ptgiam,
      giagocmin = e.giabanmin,
      giagocmax = e.giabanmax,
      giabanmin = giagocmin - (giagocmin * ptgiam) / 100,
      giabanmax = giagocmax - (giagocmax * ptgiam) / 100,
      thuesuat = e.thuesuat;
    var soluong =
      is.currentTarget.parentNode.getElementsByClassName("input_sl")[0].value;
    var thanhtienmin = giabanmin * soluong;
    var thanhtienmax = giabanmax * soluong;
    var thanhtienvatmin = thanhtienmin - (thanhtienmin * thuesuat) / 100;
    var thanhtienvatmax = thanhtienmax - (thanhtienmax * thuesuat) / 100;
    var checked_giabanmin =
      is.currentTarget.parentNode.previousSibling.getElementsByClassName(
        "checked_giabanmin"
      )[0];
    var checked_giabanmax =
      is.currentTarget.parentNode.previousSibling.getElementsByClassName(
        "checked_giabanmax"
      )[0];
    const token = localStorage;
    if (token == "block") {
      dispatch(showModal());
    } else if (token == undefined) {
      dispatch(showModal());
    } else {
      // const [ktmshh, setKtmshh] = React.useState([]);
      if (checked_giabanmin.checked == true) {
        //Xử lý khi sản phẩm có trong giỏ hàng min

        const handleKTGH = async (e) => {
          try {
            const params = {
              msdn: token,
              mshh: mshh,
              dvt: dvtmin,
            };
            const response = await dathangApi.list_kt_mshh_dathangline(params);
            if (response.length == 0) {
              const handleDathang = async (e) => {
                try {
                  const params = {
                    msdv: "",
                    msdn: token,
                    mshh: mshh,
                    tenhh: ten_sanpham,
                    dvt: dvtmin,
                    mshhnpp: mshhnpp,
                    msnpp: msnpp,
                    ptgiam: ptgiam,
                    thuesuat: thuesuat,
                    soluong: soluong,
                    gianhap: gianhap,
                    giagoc: giagocmin,
                    giaban: giabanmin,
                    thanhtien: thanhtienmin,
                    thanhtienvat: thanhtienvatmin,
                  };
                  await dathangApi.dathangline_add(params);
                  handleGiohang();
                } catch (error) {
                  console.log(error);
                }
              };
              handleDathang();
            } else {
              const handleUpdate_dathangline = async (e) => {
                const soluongNew =
                  parseInt(response[0].soluong) + parseInt(soluong);
                const thanhtienNew = giabanmin * parseInt(soluongNew);
                const thanhtienvatNew =
                  thanhtienNew - (thanhtienNew * thuesuat) / 100;
                try {
                  const params = {
                    msdn: token,
                    mshh: mshh,
                    soluong: soluongNew,
                    dvt: dvtmin,
                    thanhtien: thanhtienNew,
                    thanhtienvat: thanhtienvatNew,
                  };
                  await dathangApi.update_dathangline(params);
                  handleGiohang();
                } catch (error) {
                  console.log(error);
                }
              };
              handleUpdate_dathangline();
            }
          } catch (error) {
            console.log(error);
          }
        };
        handleKTGH();
      } else if (checked_giabanmax.checked == true) {
        //Xử lý khi sản phẩm có trong giỏ hàng max

        const handleKTGH = async (e) => {
          try {
            const params = {
              msdn: token,
              mshh: mshh,
              dvt: dvtmax,
            };
            const response = await dathangApi.list_kt_mshh_dathangline(params);
            if (response.length == 0) {
              const handleDathang = async (e) => {
                try {
                  const params = {
                    msdv: "",
                    msdn: token,
                    mshh: mshh,
                    tenhh: ten_sanpham,
                    dvt: dvtmax,
                    msnpp: msnpp,
                    mshhnpp: mshhnpp,
                    ptgiam: ptgiam,
                    thuesuat: thuesuat,
                    soluong: soluong,
                    gianhap: gianhap,
                    giagoc: giagocmax,
                    giaban: giabanmax,
                    thanhtien: thanhtienmax,
                    thanhtienvat: thanhtienvatmax,
                  };
                  await dathangApi.dathangline_add(params);
                  handleGiohang();
                } catch (error) {
                  console.log(error);
                }
              };
              handleDathang();
            } else {
              const handleUpdate_dathangline = async (e) => {
                const soluongNew =
                  parseInt(response[0].soluong) + parseInt(soluong);
                const thanhtienNew = giabanmax * parseInt(soluongNew);
                const thanhtienvatNew =
                  thanhtienNew - (thanhtienNew * thuesuat) / 100;
                try {
                  const params = {
                    msdn: token,
                    mshh: mshh,
                    soluong: soluongNew,
                    dvt: dvtmax,
                    thanhtien: thanhtienNew,
                    thanhtienvat: thanhtienvatNew,
                  };
                  await dathangApi.update_dathangline(params);
                  handleGiohang();
                } catch (error) {
                  console.log(error);
                }
              };
              handleUpdate_dathangline();
            }
          } catch (error) {
            console.log(error);
          }
        };
        handleKTGH();

        //
      } else {
        alert("Bạn chưa chọn loại hàng");
      }
    }
  }

  // Click thêm vào lượt xem
  const [ipthietbi, setIPthietbi] = useState();
  useEffect(() => {
    const handleGetLocation = () => {
      const location = navigator.geolocation;
      if (location) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else {
        document.getElementById("demo").innerHTML =
          "Geolocation is not supported by this browser.";
      }
      function showPosition(position) {
        setIPthietbi(
          position.coords.latitude + "," + position.coords.longitude
        );
      }
    };
    handleGetLocation();
  }, []);

  const handlePostLuotXem = (e, key, url) => {
    // router.replace("/sanpham");

    const msdn = localStorage;
    const mshh = hanghoa_item.mshh;
    const thietbi = navigator.appVersion;
    if (msdn) {
      const handlePost = async (e) => {
        try {
          const params = {
            msdn: msdn,
            mshh: mshh,
          };
          await hanghoaApi.post_luotxem(params);
        } catch (error) {
          console.log(error);
        }
      };
      handlePost();
    } else {
      const handlePost = async (e) => {
        try {
          const params = {
            msdn: "",
            mshh: mshh,
            ipthietbi: ipthietbi,
            thietbi: thietbi,
          };
          await hanghoaApi.post_luotxem(params);
        } catch (error) {
          console.log(error);
        }
      };
      handlePost();
    }
  };
  const handlePostTim = (e, mshh) => {
    const handlePost = async (e) => {
      try {
        const params = {
          mshh: mshh,
        };
        const response = await hanghoaApi.get_tim(params);
        const timnew = Number(response[0].tim) + 1;
        const handlePost = async (e) => {
          try {
            const params = {
              mshh: mshh,
              timnew: timnew,
            };
            await hanghoaApi.update_tim(params);
            const load_timnew = await hanghoaApi.get_tim(params);
            setTim(load_timnew[0].tim_all);
          } catch (error) {
            console.log(error);
          }
        };
        handlePost();
      } catch (error) {
        console.log(error);
      }
    };
    handlePost();
  };
  return (
    <>
      <div className="col-span-1 sanpham_item">
        <div className=" w-full rounded-[5px] shadow-sm  border-[#e0f2f9] border bg-white hover:border-[green]">
          <div>
            <Link className="img_sanpham" href={"/sanpham?" + hanghoa_item.url}>
              <Image
                className="rounded-t-[5px]"
                onClick={(e) => handlePostLuotXem(e, index, hanghoa_item.url)}
                src={require(`../../../../Backend/upload/sanpham/${hanghoa_item.path_image}`)}
                alt={hanghoa_item.tenhh}
              ></Image>
            </Link>
            <div className=" px-3 truncate">
              {/* Tên hàng hóa */}
              <input hidden className="ma_sanpham" value={hanghoa_item.mshh} />
              <input hidden id="ms_npp" value={hanghoa_item.msnpp} />
              <div className="w-full truncate">
                <Link
                  href={"/sanpham?" + hanghoa_item.url}
                  onClick={(e) => handlePostLuotXem(e, index, hanghoa_item.url)}
                  className="ten_sanpham "
                >
                  {hanghoa_item.tenhh}
                </Link>
              </div>
              <div className="chitiet_item mobile:text-xs truncate">
                {/* hàm lượng */}
                <p>
                  <span>• {hanghoa_item.msnhom}</span>
                  <span> | </span>
                  {hanghoa_item.hamluong}
                </p>
                {/* quy cách */}
                <p>
                  • {hanghoa_item.dvtmax} {hanghoa_item.slquydoi}{" "}
                  {hanghoa_item.dvtmin}
                </p>
                {/* tiêu chuẩn */}
                <p>
                  • {hanghoa_item.tieuchuan} {hanghoa_item.theodon}
                </p>
                {/* nhà phân phối */}
                <p className="chitiet_item mobile:text-xs truncate">
                  <span>• {hanghoa_item.tennhasx}</span>
                  <span> | </span>
                  <span>{hanghoa_item.msnuocsx}</span>
                </p>
                <p>
                  • <span className="ms_npp">{hanghoa_item.msnpp}</span>
                </p>
                {/* nhóm */}
                <p></p>
              </div>
              <div className="gia grid grid-cols-12">
                <div className="col-span-5 flex items-end mobile:items-start mobile:pt-2">
                  <button
                    className="flex items-center gap-2"
                    onClick={(e) =>
                      handlePostTim(hanghoa_item.tim, hanghoa_item.mshh)
                    }
                  >
                    <Image
                      src={require("../../pages/assets/img/icon/heart.png")}
                      alt=""
                    />
                    <p className="text-[#4e4e4e]">{tim}</p>
                  </button>
                </div>
                <div className="col-span-7">
                  <div className="flex gap-2 pt-2 justify-end tablet:flex-col tablet:items-end tablet:gap-0 mobile:flex-col mobile:items-end mobile:gap-0">
                    <p className="text-[gray] giagoc">
                      {hanghoa_item.giabangoc
                        .toString()
                        .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                    </p>
                    <p className="text-[black] tablet:hidden mobile:hidden">
                      -
                    </p>
                    <div className="flex gap-2">
                      <div className="flex ">
                        <p className="giaban giabanmin ">
                          {hanghoa_item.giabanmin
                            .toString()
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                        </p>
                        <p className="giaban dvt_min">/{hanghoa_item.dvtmin}</p>
                      </div>
                      <input
                        type="radio"
                        className="checked_giabanmin"
                        name={"giaban_" + hanghoa_item.mshh + index}
                        defaultChecked
                      />
                    </div>
                  </div>
                  <div className="flex gap-2  justify-end">
                    <div className="flex ">
                      <p className="giaban giabanmax">
                        {hanghoa_item.giabanmax
                          .toString()
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                      </p>
                      <p className="giaban dvt_max">/{hanghoa_item.dvtmax}</p>
                    </div>
                    <input
                      name={"giaban_" + hanghoa_item.mshh + index}
                      type="radio"
                      className="checked_giabanmax"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-12 gap-2 p-3">
              <div className="soluong">
                <button className="" onClick={(e) => soluong_giam(index, e)}>
                  <Image
                    src={require("../../pages/assets/img/icon/minus.png")}
                    alt="minus"
                  ></Image>
                </button>
                <input
                  onChange={(e) =>
                    setSoluong(e.target.value.replace(/[^0-9\.\,]/g, ""))
                  }
                  maxLength="4"
                  id="input_sl"
                  className="input_sl w-10 text-center outline-none"
                  name={hanghoa_item.mshh}
                  type="text"
                  value={soluong}
                ></input>
                <button onClick={(e) => soluong_tang(index, e)} className="">
                  <Image
                    src={require("../../pages/assets/img/icon/plus.png")}
                    alt="plus"
                  ></Image>
                </button>
              </div>
              <button
                className="btn_dathang"
                onClick={(e) => dathangline_add(index, hanghoa_item, e)}
              >
                Thêm vào giỏ
              </button>
            </div>
          </div>
        </div>
      </div>
      
    </>
  );
};

export default SanphamItem;
