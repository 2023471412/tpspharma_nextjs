import React, { useState, useEffect } from "react";
import Link from "next/link";

import hanghoaApi from "../../pages/api/hanghoaApi";
import SanphamItem from "./sanpham_item";

const SanPhamTheoNhom = (prop) => {
  const [hanghoa, setHanghoa] = useState([]);
  useEffect(() => {
    const handleHanghoa = async (e) => {
      try {
        const params = {
          msnhom: danhmuc_item.msloai,
          offset: 0,
          limit: 9,
        };
        const response = await hanghoaApi.list(params);
        setHanghoa(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleHanghoa();
  }, []);
  const danhmuc_item = prop.danhmuc_item;
  return (
    <>
      {hanghoa < 1 ? (
        ""
      ) : (
        <div className="py-3">
          <div className="flex justify-between ">
            <div className=" text-left w-full flex flex-row items-center  after:border-b-[#ddd] after:border-b-[1px] after:m-auto after:w-full after:ml-5">
              <h1 className="font-semibold text-xl flex flex-row whitespace-nowrap">
                <span className="text-[green] pr-1"> • </span>
                {danhmuc_item.tenloai}
              </h1>
            </div>
            <input id="msnhom" hidden value={danhmuc_item.msloai} />

            <Link
              href={"/toanbosanpham?" + danhmuc_item.tenloai
              .toLowerCase()
              .normalize("NFD")
              .replaceAll(/[\u0300-\u036f]/g, "")
              .replaceAll(/[đĐ]/g, (m) => (m === "đ" ? "d" : "D"))
              .replaceAll(" ", "-")}
              className="border text-center px-2 border-[#a1e9a1] rounded-full bg-green-100 whitespace-nowrap"
            >
              Xem tất cả
            </Link>
          </div>
          <div className="grid grid-cols-5 gap-5 pt-5 container_sanpham  mobile:grid-cols-2 mobile:gap-3 tablet:grid-cols-4 ">
            {hanghoa.map((hanghoa_item, key) => {
              return (
                <SanphamItem
                  hanghoa_item={hanghoa_item}
                  danhmuc_item={danhmuc_item}
                  index={key}
                />
              );
            })}
          </div>
        </div>
      )}
    </>
  );
};
export default SanPhamTheoNhom;
