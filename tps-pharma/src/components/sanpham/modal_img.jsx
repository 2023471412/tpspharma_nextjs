import React, { useState, createContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalImage$ } from "../redux/selectors";
import { hideModalImage } from "../redux/actions";
import Image from "next/image";
import hanghoaApi from "../../pages/api/hanghoaApi";

function LoginModal(prop) {
    const url_sanpham = prop.url_sanpham
  const dispatch = useDispatch();
  const [ctHanghoa, setctHanghoa] = useState([]);
  const show = useSelector(modalImage$).isShow;
  useEffect(() => {
    const handleHanghoa = async (e) => {
      try {
        const params = {
          url: url_sanpham,
        };
        const response = await hanghoaApi.listchitietsp(params);
        setctHanghoa(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleHanghoa();
  }, [url_sanpham]);
  const handleCloseModal = () => {
    dispatch(hideModalImage());
  };
  return (
    <>
      {show ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative  pointer-events-none w-[300px]">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-body relative p-4 text-center w-full">
                {ctHanghoa?.map((cthanghoa_item, key) => {
                     <div className="">
                        <p>{cthanghoa_item.tenhh}</p>
                     {/* <Image
                       className="rounded-xl"
                       src={require(`../../../../Backend/upload/sanpham/${cthanghoa_item.path_image}`)}
                       alt={cthanghoa_item.path_image}
                     /> */}
                   </div>
                })}

                <div className="text-right lg:text-right flex justify-end">
                  <button
                    onClick={handleCloseModal}
                    type="button"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    className="close_login"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

export default LoginModal;
