import React, { useState, useEffect } from "react";
import "swiper/css";
import Link from "next/link";
import hanghoaApi from "../../pages/api/hanghoaApi";
import danhmucApi from "../../pages/api/danhmucApi";
import SanPhamTheoNhom from "../sanpham/sanpham_theonhom";
import Sanphambanchay from "../sanpham/sanphambanchay";
import Image from "next/image";

function Home() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const [danhmuc, setDanhmuc] = useState([]);
  const [hot_items, setHotItem] = useState([]);
  const [dataThis, setDataThis] = useState([]);
  const [dataUser, setDataUser] = useState([]);

  var hotItem_user = hot_items;
  const [localStorage, setStatus] = useState();
  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
  }, []);
  // console.log(dataThis);
  //! Get All hàng hóa
  useEffect(() => {
    const allHanghoaHandle = async (e) => {
      try {
        const params = {};
        const response = await hanghoaApi.list_all_hanghoa(params);
        setDataThis(response);
      } catch (error) {
        console.log(error);
      }
    };
    allHanghoaHandle();
  }, []);
  useEffect(() => {
    const danhmucHandle = async (e) => {
      try {
        const params = {
          phanloai: "msnhom",
        };
        const response = await danhmucApi.listdanhmuc(params);
        setDanhmuc(response);
      } catch (error) {
        console.log(error);
      }
    };
    danhmucHandle();
  }, []);
  //todo Get sản phẩm bán chạy của từng user
  const token = localStorage;

  //!Load HotItem
  useEffect(() => {
    if (token == "block" || token == undefined) {
      const hotItemHandle = async (e) => {
        try {
          const params = { mshh: "", soluong: "15" };
          const response = await hanghoaApi.list_hot_items(params);
          setHotItem(response);
        } catch (error) {
          console.log(error);
        }
      };
      hotItemHandle();
    } else {
      const hotItemHandle = async (e) => {
        var myHeaders = new Headers();
        myHeaders.append(
          "Authorization",
          "bearer eyJ0eXAi.iJKV1QiLCJhbGci.iJIUzI1NiJ9OeyJtc2R2IjoiMjIwMjIwMTA1NDA2MzciLCJtc2RuIjoiMDkwNzY3.DIzNCIsInRlbmR2IjoiTkhcdTAwYzAgVEhVXHUxZWQwQyBBTiBUXHUwMGMyTSIsImV4cGlyZWQi.jE3MDQ4NTg5NjJ9O0PpmEeZGj754fysBTcULwhqLLqMe2l6y-0mc.ihCl.0"
        );
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append(
          "Cookie",
          "PHPSESSID=b28a98b0f4e940bf3deb8d41d2549897"
        );

        var raw = JSON.stringify({
          msdv: localStorage,
          // msdv: "test",
          soluong: 15,
        });

        var requestOptions = {
          method: "POST",
          headers: myHeaders,
          body: raw,
          redirect: "follow",
        };

        await fetch("https://egpp.vn/api_tmdt/list_nhapkho", requestOptions)
          .then((response) => response.text())
          .then((result) => setDataUser(JSON.parse(result)))
          .catch((error) => setDataUser());
      };
      hotItemHandle();
    }
  }, []);
  useEffect(() => {
    const data = () => {
      if (token != "block" || token != undefined) {
        if (dataThis != "" && dataUser != "") {
          dataUser.forEach(function data_user(itemUser, indexUser, arrayUser) {
            let sodangkyUser = arrayUser[indexUser].sodangky;
            let tenhhUSer = arrayUser[indexUser].tenhh;
            let hoatchatchinhUser = arrayUser[indexUser].hoatchatchinh;
            dataThis.forEach(function data_this(
              itemThis,
              indexThis,
              arrayThis
            ) {
              let tenhhThis = arrayThis[indexThis].tenhh;
              let mshhThis = arrayThis[indexThis].mshh;
              let sodangkyThis = arrayThis[indexThis].sodangky;
              let tenhoatchatThis =
                arrayThis[indexThis].tenhoatchat == undefined
                  ? ""
                  : arrayThis[indexThis].tenhoatchat;

              if (sodangkyUser === sodangkyThis && sodangkyUser != "") {
                return hotItem_user.push(arrayThis[indexThis]);
              }
              if (
                sodangkyUser != sodangkyThis &&
                tenhhUSer != "" &&
                tenhhThis.search(tenhhUSer) >= 0
              ) {
                return hotItem_user.push(arrayThis[indexThis]);
              }
              if (
                sodangkyUser != sodangkyThis &&
                tenhhUSer != "" &&
                hoatchatchinhUser != "" &&
                tenhoatchatThis.search(hoatchatchinhUser) >= 0
              ) {
                return hotItem_user.push(arrayThis[indexThis]);
              }
            });
          });
        } else {
          const hotItemDataUserRongHandle = async (e) => {
            try {
              const params = { mshh: "", soluong: "15" };
              const response = await hanghoaApi.list_hot_items(params);
              setHotItem(response);
            } catch (error) {
              console.log(error);
            }
          };
          hotItemDataUserRongHandle();
        }
      }
    };
    data();
  }, []);

  return (
    <>
      <div className="bg_body">
        {/* Banner */}
        <div className="tablet:px-3 mobile:px-2">
          <div className="fullscreen:container mx-auto w-full bg-transparent lglap:px-5 smlap:px-5">
            <div className="grid grid-cols-12 gap-4 mx-auto py-2 items-center ">
              <div className="col-span-8">
                <div
                  id="carouselExampleControls"
                  className="relative"
                  data-te-carousel-init
                  data-te-carousel-slide
                >
                  <div className="relative w-full overflow-hidden after:clear-both after:block after:content-[''] rounded-[1rem]">
                    <div
                      className="relative float-left -mr-[100%] w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none "
                      data-te-carousel-item
                      data-te-carousel-active
                    >
                      <Image
                        src={require("../../pages/assets/img/banner/quangcao1.jpg")}
                        className="block w-full"
                        alt="Wild Landscape"
                      />
                    </div>
                    <div
                      className="relative float-left -mr-[100%] hidden w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none"
                      data-te-carousel-item
                    >
                      <Image
                        src={require("../../pages/assets/img/banner/quangcao2.jpg")}
                        className="block w-full"
                        alt="Camera"
                      />
                    </div>
                    <div
                      className="relative float-left -mr-[100%] hidden w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none"
                      data-te-carousel-item
                    >
                      <Image
                        src={require("../../pages/assets/img/banner/quangcao3.jpg")}
                        className="block w-full"
                        alt="Exotic Fruits"
                      />
                    </div>
                  </div>
                  <button
                    className="absolute top-0 bottom-0 left-0 z-[1] flex w-[15%] items-center justify-center border-0 bg-none p-0 text-center text-white opacity-50 transition-opacity duration-150 ease-[cubic-bezier(0.25,0.1,0.25,1.0)] hover:text-white hover:no-underline hover:opacity-90 hover:outline-none focus:text-white focus:no-underline focus:opacity-90 focus:outline-none motion-reduce:transition-none"
                    type="button"
                    data-te-target="#carouselExampleControls"
                    data-te-slide="prev"
                  >
                    <span className="inline-block h-8 w-8">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        className="h-6 w-6"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          d="M15.75 19.5L8.25 12l7.5-7.5"
                        />
                      </svg>
                    </span>
                    <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
                      Previous
                    </span>
                  </button>
                  <button
                    className="absolute top-0 bottom-0 right-0 z-[1] flex w-[15%] items-center justify-center border-0 bg-none p-0 text-center text-white opacity-50 transition-opacity duration-150 ease-[cubic-bezier(0.25,0.1,0.25,1.0)] hover:text-white hover:no-underline hover:opacity-90 hover:outline-none focus:text-white focus:no-underline focus:opacity-90 focus:outline-none motion-reduce:transition-none"
                    type="button"
                    data-te-target="#carouselExampleControls"
                    data-te-slide="next"
                  >
                    <span className="inline-block h-8 w-8">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        className="h-6 w-6"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          d="M8.25 4.5l7.5 7.5-7.5 7.5"
                        />
                      </svg>
                    </span>
                    <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
                      Next
                    </span>
                  </button>
                </div>
              </div>
              <div className="col-span-4 ">
                <Image
                  className="w-auto rounded-2xl my-2"
                  src={require("../../pages/assets/img/banner/quangcao2.jpg")}
                  alt=""
                />
                <Image
                  className="w-auto rounded-2xl my-2 "
                  src={require("../../pages/assets/img/banner/quangcao3.jpg")}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>

        <div className="sessions_container mobile:pt-1 grid grid-cols-12  smlap:px-5 lglap:px-5 tablet:px-3 mobile:px-2">
          <div className="col-span-12">
            {/* Sản phẩm bán chạy*/}
            <div className="pb-[19px]">
              <div className="flex justify-between">
                <h1 className="title_sanpham">Sản phẩm bán chạy</h1>
                <Link
                  href={"/toanbobanchay"}
                  className="border px-2 border-[#a1e9a1] rounded-full bg-green-100"
                >
                  Xem tất cả
                </Link>
              </div>
              <div className="grid grid-cols-5 tablet:grid-cols-4 gap-3 pt-5 w-full mobile:grid-cols-2 mobile:gap-3 container_sanpham ">
                <Sanphambanchay
                  hanghoa_item={hotItem_user}
                  length_data_user={dataUser.length}
                />
              </div>
            </div>
            {/* Box thông tin */}
            <div className="fullscreen:container mobile:px-2 bg-[#102f49] h-auto p-[20px] rounded-lg flex  mobile:flex-wrap mobile:flex-row justify-around items-center gap-5 w-full">
              <div className="bg-[#f1fafe] rounded-md h-[200px]  mobile:w-[47%] text-center flex flex-col justify-center leading-[2.5rem]">
                <Image
                  className="mx-auto"
                  src={require("../../pages/assets/img/icon/chinhhang_64.png")}
                  alt='Chính hãng'
                ></Image>
                <p className="font-semibold text-lg py-2">Chính hãng</p>
                <p>The Earth is a very small stage in a vast</p>
              </div>
              <div className="bg-[#f1fafe] rounded-md h-[200px]  mobile:w-[47%] text-center flex flex-col justify-center leading-[2.5rem]">
                <Image
                  className="mx-auto"
                  src={require("../../pages/assets/img/icon/giatot_64.png")}
                  alt="Giá tốt"
                ></Image>
                <p className="font-semibold text-lg py-2">Giá tốt</p>
                <p>The Earth is a very small stage in a vast</p>
              </div>
              <div className="bg-[#f1fafe] rounded-md h-[200px]  mobile:w-[47%] text-center flex flex-col justify-center leading-[2.5rem]">
                <Image
                  className="mx-auto"
                  src={require("../../pages/assets/img/icon/sanhang_64.png")}
                  alt='Sẵn hàng'
                ></Image>
                <p className="font-semibold text-lg py-2">Sẵn hàng</p>
                <p>The Earth is a very small stage in a vast</p>
              </div>
              <div className="bg-[#f1fafe] rounded-md h-[200px]  mobile:w-[47%] text-center flex flex-col justify-center leading-[2.5rem]">
                <Image
                  className="mx-auto"
                  src={require("../../pages/assets/img/icon/giaongay_64.png")}
                  alt='Giao ngay'
                ></Image>
                <p className="font-semibold text-lg py-2">Tiện lợi</p>
                <p>The Earth is a very small stage in a vast</p>
              </div>
            </div>
            {/* Slide 2 */}
            <div>
              <div className="fullscreen:container mx-auto w-full bg-transparent">
                <div className="grid grid-cols-12 gap-4 mx-auto  py-2 items-center ">
                  <div className="col-span-8">
                    <div
                      id="carouselExampleControls"
                      className="relative"
                      data-te-carousel-init
                      data-te-carousel-slide
                    >
                      <div className="relative w-full overflow-hidden after:clear-both after:block after:content-[''] rounded-[1rem]">
                        <div
                          className="relative float-left -mr-[100%] w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none "
                          data-te-carousel-item
                          data-te-carousel-active
                        >
                          <Image
                            src={require("../../pages/assets/img/banner/quangcao1.jpg")}
                            className="block w-full"
                            alt="Wild Landscape"
                          />
                        </div>
                        <div
                          className="relative float-left -mr-[100%] hidden w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none"
                          data-te-carousel-item
                        >
                          <Image
                            src={require("../../pages/assets/img/banner/quangcao2.jpg")}
                            className="block w-full"
                            alt="Camera"
                          />
                        </div>
                        <div
                          className="relative float-left -mr-[100%] hidden w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none"
                          data-te-carousel-item
                        >
                          <Image
                            src={require("../../pages/assets/img/banner/quangcao3.jpg")}
                            className="block w-full"
                            alt="Exotic Fruits"
                          />
                        </div>
                      </div>
                      <button
                        className="absolute top-0 bottom-0 left-0 z-[1] flex w-[15%] items-center justify-center border-0 bg-none p-0 text-center text-white opacity-50 transition-opacity duration-150 ease-[cubic-bezier(0.25,0.1,0.25,1.0)] hover:text-white hover:no-underline hover:opacity-90 hover:outline-none focus:text-white focus:no-underline focus:opacity-90 focus:outline-none motion-reduce:transition-none"
                        type="button"
                        data-te-target="#carouselExampleControls"
                        data-te-slide="prev"
                      >
                        <span className="inline-block h-8 w-8">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="1.5"
                            stroke="currentColor"
                            className="h-6 w-6"
                          >
                            <path
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              d="M15.75 19.5L8.25 12l7.5-7.5"
                            />
                          </svg>
                        </span>
                        <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
                          Previous
                        </span>
                      </button>
                      <button
                        className="absolute top-0 bottom-0 right-0 z-[1] flex w-[15%] items-center justify-center border-0 bg-none p-0 text-center text-white opacity-50 transition-opacity duration-150 ease-[cubic-bezier(0.25,0.1,0.25,1.0)] hover:text-white hover:no-underline hover:opacity-90 hover:outline-none focus:text-white focus:no-underline focus:opacity-90 focus:outline-none motion-reduce:transition-none"
                        type="button"
                        data-te-target="#carouselExampleControls"
                        data-te-slide="next"
                      >
                        <span className="inline-block h-8 w-8">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="1.5"
                            stroke="currentColor"
                            className="h-6 w-6"
                          >
                            <path
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              d="M8.25 4.5l7.5 7.5-7.5 7.5"
                            />
                          </svg>
                        </span>
                        <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
                          Next
                        </span>
                      </button>
                    </div>
                  </div>
                  <div className="col-span-4 ">
                    <Image
                      className="w-auto rounded-2xl my-2"
                      src={require("../../pages/assets/img/banner/quangcao2.jpg")}
                      alt='Banner 2'
                    />
                    <Image
                      className="w-auto rounded-2xl my-2 "
                      src={require("../../pages/assets/img/banner/quangcao3.jpg")}
                      alt='Banner 3'
                    />
                  </div>
                </div>
              </div>
            </div>
            {/* Load group sản phẩm nổi bật */}
            <div className=" grid grid-cols-5 tablet:grid-cols-4 pt-5 gap-3 container_sanpham  mobile:grid-cols-2 mobile:gap-3 mobile:pt-2">
              <div className="col-span-1">
                <div className=" w-full  border bg-white hover:border-[green]">
                  <div>
                    <Link href="/sanpham" className="img_sanpham">
                      <Image
                        src={require("../../pages/assets/img/anhsanpham/anhsanpham.jpg")}
                        alt="anhsanpham"
                      />
                    </Link>
                    <div className=" px-3">
                      {/* Tên hàng hóa */}
                      <Link href="/sanpham" className="ten_sanpham">
                        Thực phẩm bảo vệ sức khỏe bổ mắt
                      </Link>
                      <div className=" chitiet_item">
                        {/* hàm lượng */}
                        <p>
                          <span>• Kháng sinh</span>
                          <span> | </span>
                          250mg
                        </p>
                        {/* quy cách */}
                        <p>• Hộp 24 gói x 1,5 g</p>
                        {/* tiêu chuẩn */}
                        <p>
                          • GMP
                          <span> | </span>
                          <span>GMPWO</span>
                          <span> | </span>
                          <span> GMPEU</span>
                        </p>
                        {/* nhà phân phối */}
                        <p className=" chitiet_item">
                          <span>• DHG</span>
                          <span> | </span>
                          <span>Việt Nam</span>
                        </p>
                        {/* nhóm */}
                        <p></p>
                      </div>
                      <div className="flex gap-2 pt-2">
                        <p className="text-[gray] giagoc">129.500</p>
                        <p className="text-[black]">-</p>
                        <div className="flex">
                          <p className="giaban">119.000</p>
                          <p className="giaban">/chai</p>
                        </div>
                      </div>
                    </div>
                    <div className="grid grid-cols-12 gap-2 p-3">
                      <div className="soluong">
                        <button className="">
                          <Image
                            src={require("../../pages/assets/img/icon/minus.png")}
                            alt="minus"
                          />
                        </button>
                        <input
                          className="w-10 text-center outline-none"
                          value={"10"}
                          readOnly
                        ></input>
                        <button className="">
                          <Image
                            src={require("../../pages/assets/img/icon/plus.png")}
                            alt="plus"
                          />
                        </button>
                      </div>
                      <button className="btn_dathang">Thêm vào giỏ</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Sản phẩm giá tốt*/}
            <div className="pt-[20px] mobile:px-2">
              <h1 className=" title_sanpham ">Sản phẩm giá tốt</h1>
              {danhmuc?.map((danhmuc_item, key) => {
                return (
                  <SanPhamTheoNhom danhmuc_item={danhmuc_item} index={key} />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
