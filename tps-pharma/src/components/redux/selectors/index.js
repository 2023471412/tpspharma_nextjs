export const giohangState$ = (state) => state.giohang;
export const filterState$ = (state) => state.filter;
export const modalLoginState$ = (state) => state.modal;
export const modalImage$ = (state) => state.modalImage;
export const checkedState$ = (state) => state.checked;
