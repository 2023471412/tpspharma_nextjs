import { combineReducers } from "redux";
import giohang from "./giohang";
import filter from "./filter";
import modal from "./modal";
import modalImage from "./modalImage";
import checked from "./checked";

export default combineReducers({
  giohang,
  filter,
  modal,
  modalImage,
  checked,
});
