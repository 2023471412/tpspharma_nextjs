import { createActions, createAction } from "redux-actions";

export const getType = (reduxAction) => {
  return reduxAction().type;
};

//todo push gio hang
export const createGiohang = createActions({
  createGiohangRequest: (payload) => payload,
  createGiohangSuccess: (payload) => payload,
  createGiohangFailure: (err) => err,
});
//todo push gio hang
export const createFilter = createActions({
  createFilterRequest: (payload) => payload,
  createFilterSuccess: (payload) => payload,
  createFilterFailure: (err) => err,
});
export const showModal = createAction("SHOW_MODAL");
export const hideModal = createAction("HIDE_MODAL");

export const showModalImage = createAction("SHOW_MODAL_IMG");
export const hideModalImage = createAction("HIDE_MODAL_IMG");

export const isChecked = createAction("IS_CHECK");
export const hideCheck = createAction("HIDE_CHECK");
