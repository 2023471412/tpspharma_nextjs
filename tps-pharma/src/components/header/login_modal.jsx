import React, { useState, createContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalLoginState$ } from "../redux/selectors";
import { hideModal } from "../redux/actions";
import Image from "next/image";

function LoginModal() {
  const dispatch = useDispatch();
  const show = useSelector(modalLoginState$).isShow;
  const [openWarning, setOpenWarning] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  const handleCloseModal = () => {
    dispatch(hideModal());
    setOpenWarning(false);
  };
  return (
    <>
      {show ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative  pointer-events-none w-[300px]">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-body relative p-4 text-center w-full">
                <div className="flex justify-between items-center mb-6">
                  <a className="text-black w-full">
                    <p>Đăng nhập nhanh bằng </p>
                    <p
                      className="text-center pt-[20px] hover:cursor-pointer"
                      onClick={(e) =>
                        (window.location.href = "https://egpp.vn/")
                      }
                    >
                      <Image
                        className="m-auto"
                        src={require("../../pages/assets/img/icon/logo_ephamarcy.png")}
                      ></Image>
                    </p>
                  </a>
                </div>

                <div className="text-right lg:text-right flex justify-end">
                  <button
                    onClick={handleCloseModal}
                    type="button"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    className="close_login"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

export default LoginModal;
