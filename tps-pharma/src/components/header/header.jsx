import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import LoginForm from "./login_form";
import Search from "./search_form";
import danhmucApi from "../../pages/api/danhmucApi";
import hangsxApi from "../../pages/api/hangsxApi";
import hanghoaApi from "../../pages/api/hanghoaApi";
import { useDispatch, useSelector } from "react-redux";
import { createGiohang, createFilter } from "../redux/actions";
import { giohangState$ } from "../redux/selectors";
import { showModal } from "../redux/actions";
import { useRouter } from "next/router";
import LoginModal from "./login_modal";

function Header() {
  const [localStorage_Tendv, setTendv] = useState();
  const [tieuchuan, settieuchuan] = useState([]);
  const [nuocsx, setNuocsx] = useState([]);
  const [hangsx, setHangsx] = useState([]);
  const [danhmuc, setDanhmuc] = useState([]);
  const [sodienthoai, setSDT] = useState([]);
  const [localStorage, setStatus] = useState();
  const dispatch = useDispatch();
  const giohang = useSelector(giohangState$).length;
  const router = useRouter();
  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
    setTendv(window.localStorage.getItem("tendv"));
  }, []);
  useEffect(() => {
    //todo Danh muc nhom san pham
    const danhmucHandle = async (e) => {
      try {
        const params = {
          msdv: "",
          phanloai: "msnhom",
        };
        const response = await danhmucApi.listdanhmuc(params);
        setDanhmuc(response);
      } catch (error) {
        console.log(error);
      }
    };
    danhmucHandle();
    //todo danh muc hang sx
    const handleHangsx = async (e) => {
      try {
        const params = {
          loai: "hsx",
        };
        const response = await hangsxApi.listhangsx(params);
        setHangsx(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleHangsx();
    //todo danh muc nuoc san xuat
    const handleNuocsx = async (e) => {
      try {
        const params = {
          msdv: "",
          phanloai: "nuocsx",
        };
        const response = await danhmucApi.listdanhmuc(params);
        setNuocsx(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleNuocsx();
    //todo Danh  muc tieu chuan
    const handleTieuchuan = async (e) => {
      try {
        const params = {
          msdv: "",
          phanloai: "tieuchuan",
        };
        const response = await danhmucApi.listdanhmuc(params);
        settieuchuan(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleTieuchuan();
    //todo load Giỏ hàng trên header
    // const handleGiohang = (e) => {
    //   const params = {
    //     msdn: localStorage,
    //   };
    //   dispatch(createGiohang.createGiohangRequest(params));
    // };
    // handleGiohang();
    //todo Load số điện thoại
    const handleSDT = async (e) => {
      try {
        const params = {
          phanloai: "hotline",
        };
        const response = await danhmucApi.sodienthoai(params);
        setSDT(response[0].tenloai);
      } catch (error) {
        console.log(error);
      }
    };
    handleSDT();
  }, []);
  //todo load Giỏ hàng trên header
  useEffect(() => {
    function handleGiohang(e) {
      const params = {
        msdn: localStorage,
      };
      dispatch(createGiohang.createGiohangRequest(params));
    }
    handleGiohang();
  }, [localStorage]);
  //todo active class bộ lọc
  const active_filter = (e, type_filter) => {
    switch (type_filter) {
      case "hang":
        if (e.target.classList.contains("active_filter")) {
          e.target.classList.remove("active_filter");
        } else {
          e.target.classList.add("active_filter");
        }
        break;
      case "tieuchuan":
        if (e.target.classList.contains("active_filter")) {
          e.target.classList.remove("active_filter");
        } else {
          e.target.classList.add("active_filter");
        }
        break;
      case "nuoc":
        if (e.target.classList.contains("active_filter")) {
          e.target.classList.remove("active_filter");
        } else {
          e.target.classList.add("active_filter");
        }
        break;
      case "nhom":
        if (e.target.classList.contains("active_nhomsp")) {
          e.target.classList.remove("active_nhomsp");
        } else {
          e.target.classList.add("active_nhomsp");
        }
        break;
      case "/":
        const nhomsp = document.querySelectorAll(".items_nhomsp li");
        nhomsp.forEach((nhomsp) => {
          nhomsp.classList.remove("active_nhomsp");
        });
        const hangsx = document.querySelectorAll(".items_hangsx li ");
        hangsx.forEach((hangsx) => {
          hangsx.classList.remove("active_filter");
        });
        const tieuchuan = document.querySelectorAll(".items_tieuchuan li ");
        tieuchuan.forEach((tieuchuan) => {
          tieuchuan.classList.remove("active_filter");
        });
        const nuocsx = document.querySelectorAll(".items_nuocsx li ");
        nuocsx.forEach((nuocsx) => {
          nuocsx.classList.remove("active_filter");
        });
        router.push("/");

        break;
      default:
        break;
    }
  };

  //todo filter lọc sản phẩm
  const pathSearch = router.asPath.split("?")[1];
  const handleFilter = (url_filter) => {
    const search = JSON.stringify(router.query)
      .replace("{", "")
      .replace("}", "")
      .replaceAll('"', "")
      .replaceAll(":", "=")
      .replaceAll(",", "&");
    const valueSearch =
      (search ? "?" + pathSearch.replaceAll('"', "") + "&" : search + "?") +
      url_filter;
    router.push("/filter" + valueSearch);
    if (pathSearch) {
      const paramsFilter = pathSearch.split("&");
      console.log(paramsFilter);
      for (var i = 0; i < paramsFilter.length; i++) {
        if (paramsFilter[i] == url_filter) {
          const new_arr = paramsFilter.filter((item) => item !== url_filter);
          console.log(new_arr);
          router.push("/filter" + (search ? "?" + new_arr : search + "?"));
          if (paramsFilter.length > 2) {
            const new_nav = new_arr.toString().replaceAll(",", "&");
            router.push("/filter?" + new_nav);
          }
        }
      }
    }
  };
  useEffect(() => {
    const filterHandle = async (e) => {
      if (pathSearch) {
        const valueFilter = pathSearch.split("&");
        const nhom_arr = [];
        const msnhasx_arr = [];
        const tieuchuan_arr = [];
        const nuoc_arr = [];
        sosanh("msnhom", nhom_arr);
        sosanh("msnhasx", msnhasx_arr);
        sosanh("tieuchuan", tieuchuan_arr);
        sosanh("nuoc", nuoc_arr);
        function sosanh(key_sosanh, arr) {
          for (let i = 0; i < valueFilter.length; i++) {
            var sosanh = new RegExp(key_sosanh, "i");
            if (valueFilter[i].search(sosanh) >= 0) {
              arr.push("'" + valueFilter[i].split("=")[1] + "'");
            }
          }
        }
        var params = {
          msnhom: nhom_arr,
          msnhasx: msnhasx_arr,
          tieuchuan: tieuchuan_arr,
          nuoc: nuoc_arr,
        };
        dispatch(createFilter.createFilterRequest(params));
      }
    };
    filterHandle();
  }, [pathSearch]);
  //todo open menu Mobile
  function toggleMenu() {
    var menu = document.getElementById("menu");
    var bg_menu = document.getElementById("bg_menu");
    menu.classList.toggle("hidden");
    menu.classList.toggle("w-[70%]");
    menu.classList.toggle("h-screen");
    bg_menu.classList.toggle("hidden");
    bg_menu.classList.toggle("w-full");
    bg_menu.classList.toggle("h-screen");
  }
  //todo close menu Mobile

  function toggleMenuClose() {
    var menu = document.getElementById("menu");
    var bg_menu = document.getElementById("bg_menu");
    menu.classList.toggle("hidden");
    menu.classList.toggle("w-full");
    menu.classList.toggle("h-screen");
    bg_menu.classList.toggle("hidden");
    bg_menu.classList.toggle("w-full");
    bg_menu.classList.toggle("h-screen");

    dispatch(showModal());
  }
  useEffect(() => {
    const handleGetLocation = () => {
      const location = navigator.geolocation;
      const thietbi = navigator.appVersion;
      if (location) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else {
        document.getElementById("demo").innerHTML =
          "Geolocation is not supported by this browser.";
      }
      function showPosition(position) {
        let ipthietbi =
          position.coords.latitude + "," + position.coords.longitude;
        const msdn = localStorage;
        if (msdn) {
          const handleSetViTri = async (e) => {
            try {
              const params = {
                msdn: msdn,
                ipthietbi: ipthietbi,
                thietbi: thietbi,
                mshh: "",
                tim: "",
              };
              await hanghoaApi.post_luotxem(params);
            } catch (error) {
              console.log(error);
            }
          };
          handleSetViTri();
        } else {
          const handleSetViTri = async (e) => {
            try {
              const params = {
                msdn: "",
                ipthietbi: ipthietbi,
                thietbi: thietbi,
                mshh: "",
                tim: "",
              };
              await hanghoaApi.post_luotxem(params);
            } catch (error) {
              console.log(error);
            }
          };
          handleSetViTri();
        }
      }
    };
    handleGetLocation();
  }, []);
  return (
    <>
      <div className="sticky z-[999]  top-0 shadow-md">
        <div className="bg-white w-auto h-auto  ">
        <div className=" fullscreen:mx-10 smlap:mx-10 lglap:container mx-auto text-black py-2 tablet:px-3">
            <div className="grid grid-cols-6 gap-5 mobile:px-2 mobile:gap-1 ">
              <div className="mobile:col-span-1 mobile:flex smlap:hidden mobile:justify-start fullscreen:hidden lglap:hidden tablet:hidden mobile:items-center">
                <div className="flex items-center md:hidden ">
                  <button
                    onClick={toggleMenu}
                    className="text-white text-4xl font-bold hover:opacity-100 duration-300"
                  >
                    <Image
                      src={require("../../pages/assets/img/icon/menu.jpg")}
                      alt='menu'
                    />
                  </button>
                </div>
                <div>
                  <div
                    id="bg_menu"
                    onClick={toggleMenu}
                    className="hidden fixed top-0 left-0 px-10 py-16 bg-[#000] opacity-50 z-50
                    md:relative md:flex md:w-[70%] md:p-0 md:bg-transparent md:flex-row md:space-x-6"
                  ></div>
                  <ul
                    id="menu"
                    className="hidden fixed top-0 left-0 px-5 py-16 bg-[#fff] z-[59]
                    md:relative md:flex md:w-[70%] md:p-0 md:bg-transparent md:flex-row md:space-x-6"
                  >
                    <li className="md:hidden z-90 fixed top-0 left-0 w-[70%] bg-green-700 flex justify-between items-center">
                      <div className="text-left text-white ml-5">
                        {localStorage ? (
                          <p>{localStorage_Tendv}</p>
                        ) : (
                          <p
                            onClick={toggleMenuClose}
                            className="underline underline-offset-1"
                          >
                            Đăng nhập
                          </p>
                        )}
                      </div>
                      <p
                        onClick={toggleMenu}
                        className="text-right text-white text-4xl mr-2"
                      >
                        &times;
                      </p>
                    </li>

                    <li>
                      <Link
                        onClick={toggleMenu}
                        className="text-black hover:opacity-100 duration-300 flex gap-2"
                        href="/"
                      >
                        <div>
                          <Image
                            src={require("../../pages/assets/img/icon/home.png")}
                            alt="Home"
                          />
                        </div>
                        <p>Home</p>
                      </Link>
                    </li>
                    {localStorage ? (
                      <li>
                        <div className="text-black hover:opacity-100 duration-300 flex gap-2">
                          <div>
                            <Image
                              src={require("../../pages/assets/img/icon/logout.png")}
                              alt="Logout"
                            />
                          </div>
                          <p>Đăng xuất</p>
                        </div>
                      </li>
                    ) : (
                      ""
                    )}
                  </ul>
                </div>
              </div>

              <div className="col-span-1 mobile:flex mobile:items-center tablet:flex tablet:items-center">
                <button onClick={(e) => active_filter(e, "/")}>
                  <Image
                    className="max-w-full mobile:hidden"
                    src={require("../../pages/assets/img/banner/Logo_TPSPharma.png")}
                    alt='Logo TPS Pharma'
                  ></Image>
                  <Image
                    className="max-w-full smlap:hidden fullscreen:hidden tablet:hidden lglap:hidden"
                    src={require("../../pages/assets/img/banner/Logo_TPSPharma_mini.png")}
                    alt='Logo TPS Pharma mobile'
                  ></Image>
                </button>
              </div>
              {/* Search */}
              <Search />
              <div className="col-span-2 lglap:col-span-3 mobile:col-span-1">
                <ul className="flex gap-2 h-10  items-center justify-end  text-[black]">
                  <li className="flex gap-2 items-center  mobile:hidden">
                    {/* <Image src={require("../../pages/assets/img/icon/phone.png")} /> */}
                    <p>{sodienthoai} • </p>
                  </li>
                  <li className=" mobile:hidden">
                    <LoginForm />
                  </li>
                  <li>
                    {localStorage && localStorage !='block' ? (
                      <Link href="/giohang">
                        <div className="flex relative">
                          <button className=" rounded-md text-[red] tablet:hidden mobile:hidden">
                            GIỎ HÀNG ({giohang})
                          </button>
                          <button className=" smlap:hidden rounded-md text-[red] fullscreen:hidden lglap:hidden mobile:relative ">
                            <p className="mobile:absolute mobile:right-[-6px] mobile:top-[-14px] mobile:text-sm tablet:absolute tablet:right-[-6px] tablet:top-[-14px] tablet:text-sm">
                              ({giohang})
                            </p>
                            <Image
                              className=""
                              src={require("../../pages/assets/img/icon/cart_mobile.png")}
                              alt='Giỏ hàng Mobile'
                            />{" "}
                          </button>
                        </div>
                      </Link>
                    ) : (
                      <Link href="#">
                        <div className="flex relative">
                          <button className=" rounded-md  tablet:hidden mobile:hidden">
                            GIỎ HÀNG ({giohang})
                          </button>
                          <button className="smlap:hidden rounded-md  fullscreen:hidden lglap:hidden mobile:relative ">
                            <p className="mobile:absolute mobile:right-[-6px] mobile:top-[-14px] mobile:text-sm tablet:absolute tablet:right-[-6px] tablet:top-[-14px] tablet:text-sm">
                              ({giohang})
                            </p>
                            <Image
                              className=""
                              src={require("../../pages/assets/img/icon/cart_mobile.png")}
                              alt='Giỏ hàng Mobile'
                            />{" "}
                          </button>
                        </div>
                      </Link>
                    )}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        {/* {check_page == sanpham} */}
        {/* Danh mục nhóm sản phẩm */}
        <div className="bg-[#103c19] ">
        <ul className="items_nhomsp fullscreen:container smlap:container lglap:container mx-auto text-[14px] h-[40px] flex justify-between items-center text-white tablet:text-sm tablet:overflow-x-scroll tablet:whitespace-nowrap tablet:gap-3 tablet:px-3 mobile:overflow-x-scroll mobile:whitespace-nowrap mobile:gap-2">
            {danhmuc.map((danhmuc_item, key) => {
              let tenviettat_filter =
                "msnhom=" +
                danhmuc_item.msloai
                  .replaceAll(" ", "-")
                  .normalize("NFD")
                  .replace(/[\u0300-\u036f]/g, "")
                  .replace(/[đĐ]/g, (m) => (m === "đ" ? "d" : "D"))
                  .toLowerCase();
              const handleActive_filter = (e) => {
                active_filter(e, "nhom");
                handleFilter(tenviettat_filter);
              };
              return (
                <li
                  onClick={(e) => handleActive_filter(e)}
                  className="h-full flex items-center hover:cursor-pointer"
                >
                  {danhmuc_item.tenloai}
                </li>
              );
            })}
          </ul>
        </div>
        {/* Filter */}
        <div id="filter_sanpham" className="filter_sanpham">
          {/* Hãng sản xuất */}
          <div className="bg-[#f1fafe] ">
          <ul className="items_hangsx fullscreen:container smlap:container lglap:container mx-auto h-[40px] flex justify-between items-center text-black ">
              {hangsx.map((hangsx_item, key) => {
                let tenviettat_filter =
                  "msnhasx=" +
                  hangsx_item.msnsx
                    .replaceAll(" ", "-")
                    .normalize("NFD")
                    .replace(/[\u0300-\u036f]/g, "")
                    .replace(/[đĐ]/g, (m) => (m === "đ" ? "d" : "D"))
                    .toLowerCase();
                const handleActive_filter = (e) => {
                  active_filter(e, "hang");
                  handleFilter(tenviettat_filter);
                };
                return (
                  <li
                    onClick={(e) => handleActive_filter(e)}
                    className=" hover:bg-orange-400 px-2 hover:cursor-pointer rounded-md text-sm mobile:text-xs"
                  >
                    {hangsx_item.tenviettat}
                  </li>
                );
              })}
            </ul>
          </div>
          {/* Tiêu chuẩn */}
          <div className="bg-[#f1fafe] list_nsx_nuocsx">
          <div className="flex fullscreen:container smlap:container lglap:container mx-auto">
              <ul className="items_tieuchuan fullscreen:container smlap:container lglap:container mx-auto h-[40px] gap-5 mobile:gap-2 flex justify-start items-center text-black ">

                {tieuchuan.map((tieuchuan_item, key) => {
                  let tenviettat_filter =
                    "tieuchuan=" +
                    tieuchuan_item.tenloai
                      .replaceAll(" ", "-")
                      .normalize("NFD")
                      .replace(/[\u0300-\u036f]/g, "")
                      .replace(/[đĐ]/g, (m) => (m === "đ" ? "d" : "D"))
                      .toLowerCase();
                  const handleActive_filter = (e) => {
                    active_filter(e, "tieuchuan");
                    handleFilter(tenviettat_filter);
                  };
                  return (
                    <li
                      onClick={(e) => handleActive_filter(e)}
                      className=" hover:bg-orange-400 hover:cursor-pointer px-2 rounded-md text-sm mobile:text-xs"
                    >
                      {tieuchuan_item.tenloai}
                    </li>
                  );
                })}
              </ul>
              <ul className=" items_nuocsx fullscreen:container smlap:container lglap:container mx-auto h-[40px] gap-5 mobile:gap-2 flex justify-end items-center text-black ">
                {nuocsx.map((nuocsx_item, key) => {
                  let tenviettat_filter =
                    "nuoc=" +
                    nuocsx_item.msloai
                      .replaceAll(" ", "-")
                      .normalize("NFD")
                      .replace(/[\u0300-\u036f]/g, "")
                      .replace(/[đĐ]/g, (m) => (m === "đ" ? "d" : "D"))
                      .toLowerCase();
                  const handleActive_filter = (e) => {
                    active_filter(e, "nuoc");
                    handleFilter(tenviettat_filter);
                  };
                  return (
                    <li
                      onClick={(e) => handleActive_filter(e)}
                      className=" hover:bg-orange-400 hover:cursor-pointer px-2 rounded-md text-sm mobile:text-xs"
                    >
                      {nuocsx_item.tenloai}
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
      <LoginModal/>
    </>
  );
}

export default Header;
