import React, { useState, useEffect } from "react";
import userApi from "../../pages/api/userApi";
import { useDispatch } from "react-redux";
import { createGiohang } from "../redux/actions";
import { useRouter } from "next/router";

function LoginForm() {
  const router = useRouter();
  const [localStorage, setStatus] = useState();
  const [localStorage_Tendv, setTendv] = useState();
  const [openModal, setOpenModal] = useState(false);
  
  const [openWarning, setOpenWarning] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
    setTendv(window.localStorage.getItem("tendv"));
  }, []);
  function handleGiohang(e) {
    const params = {
      msdn: localStorage.getItem("msdn"),
    };
    dispatch(createGiohang.createGiohangRequest(params));
  }
  // login
  const handleLogin = async (e) => {
    var user = document.getElementById("msdn").value;
    var pass = document.getElementById("matkhau").value;
    try {
      const params = {
        user: user,
        pass: pass,
      };
      const response = await userApi.login(params);
      // Chỉnh lại câu if khi có dữ liệu chính thức
      if (response.length != "") {
        setOpenModal(false);
        setOpenWarning(false);
        router.push("/");
        cookies.set("token", response.token);
        cookies.set("msdv", response.data[0].msdv);
        cookies.set("tendv", response.data[0].tendv);
        cookies.set("msdn", response.data[0].msdn);
        cookies.set("tendaidien", response.data[0].tennguoidaidien);
        cookies.set("diachi", response.data[0].diachi);
        cookies.set("dienthoai", response.data[0].dienthoai);
        handleGiohang();
      } else {
      }
    } catch (error) {
      setOpenWarning(true);
    }
  };
  //Logout
  const handleLogout = () => {
    cookies.remove("token");
    cookies.remove("msdv");
    cookies.remove("tendv");
    cookies.remove("msdn");
    cookies.remove("tendaidien");
    cookies.remove("diachi");
    cookies.remove("dienthoai");
    handleGiohang();
  };
  const handleCloseModal = () => {
    setOpenModal(false);
    setOpenWarning(false);
  };
  return (
    <>
      {localStorage != "block" ? (
        <div className="flex justify-center">
          <div>
            <div className="dropdown relative">
              <button
                className="dropdown-toggle py-2.5 text-[#025814] font-medium leading-tight uppercase rounded transition duration-150 ease-in-out flex items-center
                              whitespace-nowrap
                            "
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                {localStorage_Tendv} •
              </button>
            </div>
          </div>
        </div>
      ) : (
        <button
          className="uppercase"
          onClick={(e) => (window.location.href = "https://egpp.vn/")}
          type="button"
        >
          Đăng nhập bằng egpp.vn •
        </button>
      )}
      {openModal ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative w-auto pointer-events-none">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-header flex flex-shrink-0 items-center justify-between p-4 border-b border-gray-200 rounded-t-md">
                <h5
                  className="text-xl font-medium leading-normal text-gray-800"
                  id="exampleModalLabel"
                >
                  Đăng nhập bằng tài khoản nhà thuốc (egpp.vn)
                </h5>
                <button
                  onClick={handleCloseModal}
                  type="button"
                  className="btn-close box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>

              <div className="modal-body relative p-4">
                {openWarning ? (
                  <p className="text-[red]">Tài khoản không tồn tại</p>
                ) : (
                  ""
                )}
                <div className="mb-6">
                  <input
                    onChange={(e) => e.target.value}
                    type="text"
                    className="input_login"
                    id="msdn"
                    placeholder="Số điện thoại"
                  />
                </div>

                <div className="mb-6">
                  <input
                    onChange={(e) => e.target.value}
                    type="password"
                    className="input_login"
                    id="matkhau"
                    placeholder="Password"
                  />
                </div>

                <div className="flex justify-between items-center mb-6">
                  <a
                    href="https://egpp.vn/tpspharma_login"
                    className="text-gray-800"
                  >
                    Đăng nhập nhanh bằng egpp.vn
                  </a>
                </div>

                <div className="text-center lg:text-left flex justify-between">
                  <button
                    onClick={handleLogin}
                    type="submit"
                    className="btn_login"
                  >
                    Đăng nhập
                  </button>
                  <button
                    onClick={handleCloseModal}
                    type="button"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    className="close_login"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

export default LoginForm;
