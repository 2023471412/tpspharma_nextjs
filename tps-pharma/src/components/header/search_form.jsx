import { useEffect, useState, useRef } from "react";
import hanghoaApi from "../../pages/api/hanghoaApi";
import Image from "next/image";
import { useRouter } from "next/router";

function Search() {
  const router = useRouter();

  //   const [searchValue, setSearchValue] = useState("");
  const [searchResult, setSearchResult] = useState([]);
  const [showResult, setShowResult] = useState(false);

  function useDebounce(value, delay) {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
      const handler = setTimeout(() => setDebouncedValue(value), delay);

      return () => clearTimeout(handler);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);

    return debouncedValue;
  }
  const [requestSearch, setRequestSearch] = useState("");
  const [searchValue, setSearchValue] = useState([]);

  const debounced = useDebounce(requestSearch, 500);
  const inputRef = useRef();
  const searchInput = (e) => {
    const valSeach = e.target.value;
    if (!valSeach.startsWith(" ")) {
      setRequestSearch(valSeach);
    }
  };
  useEffect(() => {
    if (!debounced.trim()) {
      setSearchResult([]);
      return;
    }

    const handleSearch = async (e) => {
      try {
        const params = {
          value: debounced,
        };
        const response = await hanghoaApi.list_search(params);
        setSearchValue(response);
      } catch (error) {
        console.log(error);
      }
    };
    handleSearch();
  }, [debounced]);
  const handleClear = () => {
    setRequestSearch("");
    setSearchValue([]);
    inputRef.current.focus();
  };
  const handleHideResult = () => {
    setTimeout(function () {
      const addHidden = document.getElementById("form_Search");
      addHidden.classList.add("hidden");
    }, 500);
  };
  const handleShowResult = () => {
    const removeHidden = document.getElementById("form_Search");
    removeHidden.classList.remove("hidden");
  };
  return (
    <>
      <div className="col-span-3 lglap:col-span-2 mobile:col-span-3">
        <div className="flex w-full justify-center fullscreen:justify-end">
          <div className="w-full fullscreen:flex fullscreen:justify-center">
            <div
              onBlur={handleHideResult}
              className="relative flex flex-wrap items-stretch w-full  fullscreen:w-[35%]"
            >
              <input
                ref={inputRef}
                onChange={(e) => searchInput(e)}
                onFocus={() => handleShowResult()}
                value={requestSearch}
                className="input_search h-10 rounded-3xl"
                placeholder="Bạn cần tìm tên thuốc hoặc hoạt chất"
                aria-label="Search"
                aria-describedby="button-addon2"
              />
              {!!requestSearch && (
                <button className="btn_search" onClick={handleClear}>
                  <Image
                    src={require("../../pages/assets/img/icon/delete.png")}
                  />
                </button>
              )}
              {/* Form result Search */}
              <div id="form_Search">
                {searchValue != "" ? (
                  <ul id="form_resultSearch" className="list_search">
                    {searchValue.map((search_item, key) => {
                      return (
                        <li
                          onClick={() =>
                            router.push("/sanpham?" + search_item.url)
                          }
                          className="list_search_items"
                        >
                          <div className="col-span-2 mobile:col-span-4">
                            <Image
                              className="rounded-sm"
                              src={require(`../../../../Backend/upload/sanpham/${search_item.path_image}`)}
                              alt=""
                            />
                          </div>
                          <div className="col-span-10 mobile:text-xs mobile:col-span-8">
                            <p>
                              {search_item.tenhh} ({search_item.quycach})
                            </p>
                            <p>{search_item.tenloai}</p>
                            <p>
                              {search_item.giabanmin
                                .toString()
                                .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                              /{search_item.dvtmin} -{" "}
                              {search_item.giabanmax
                                .toString()
                                .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                              /{search_item.dvtmax}
                            </p>
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Search;
