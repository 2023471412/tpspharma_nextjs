// import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";

const Footer = () => {
  const [loading, setLoading] = useState(false);
  const [localStorage, setStatus] = useState();
  const [localStorage_New, setMsdnNew] = useState();
  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
  }, []);
  useEffect(() => {
    // auto login
    if (localStorage == "block") {
    }
    setMsdnNew();
    const tokennew = window.localStorage.getItem("msdn");
    function a() {
      if (!tokennew) {
        window.location.href = "https://egpp.vn/tpspharma_login";
        // window.location.href =
        //   "https://192.168.1.7/WebApp_Project/EPECWeb/tpspharma_login";
      }
    }
    setTimeout(a, 1000);
  }, [localStorage_New]);
  const load = () => {
    setLoading(false);
  };
  setTimeout(load, 2000);
  return (
    <>
      {loading ? (
        <div className="bg-[#ebebebb5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative  pointer-events-none w-[300px]">
            <div className="modal-content border-none relative flex flex-col w-full pointer-events-auto  bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-body relative p-4 text-center w-full">
                <div className="relative w-full h-full">
                  <Image
                    src={require("../../pages/assets/img/gif/loading.gif")}
                  ></Image>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}

      <div className="bg-white">
        <div className="fullscreen:container mx-auto">
          <div className="grid grid-cols-12 mobile:grid-cols-4 px-20 py-5 tablet:px-3  mobile:px-2 mobile:gap-2 ">
            <div className="col-span-4 flex justify-center">
              <ul className="item_footer">
                <li>
                  <h1 className="title_item_footer">Về TPSPharma</h1>
                </li>
                <li>
                  <Link href="/thongtin?gioi-thieu">Giới thiệu</Link>
                </li>
                <li>
                  <Link href="/thongtin?pham-vi-cung-cap">
                    Phạm vi cung cấp
                  </Link>
                </li>
                <li>
                  <Link href="/thongtin?chinh-sach-doi-tra">
                    Chính sách đổi trả
                  </Link>
                </li>
                <li>
                  <Link href="/thongtin?chinh-sach-giao-hang">
                    Chính sách giao hàng
                  </Link>
                </li>
                <li>
                  <Link href="/thongtin?chinh-sach-bao-mat">
                    Chính sách bảo mật
                  </Link>
                </li>
                <li>
                  <Link href="/thongtin?chinh-sach-thanh-toan">
                    Chính sách thanh toán
                  </Link>
                </li>
                <li>
                  <Link href="/thongtin?chinh-sach-thanh-vien">
                    Chính sách thành viên
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-span-4 flex justify-center">
              <ul className="item_footer">
                <li>
                  <h1 className="title_item_footer">Đường đến TPSPharma</h1>
                </li>
              </ul>
            </div>
            <div className="col-span-4 flex justify-center">
              <ul className="item_footer">
                <li>
                  <h1 className="title_item_footer">Thông tin từ TPSPharma</h1>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      {/* Footer bottom */}
      <div className="border-t-[1px] border-[#ddd]">
        <div className="container mx-auto px-20 py-2">
          <div className="text-center text-[14px]">
            Copyright © 2022 by Công ty Cổ phần TPSPharma
          </div>
        </div>
      </div>
    </>
  );
};
export default Footer;
