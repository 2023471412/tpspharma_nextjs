import React, { useState, useEffect } from "react";
import dathangApi from "../../pages/api/dathangApi";
import { useDispatch } from "react-redux";
import { createGiohang } from "../redux/actions";
import Image from "next/image";

const GioHangItem = (prop) => {
  const giohangItem = prop.giohangItem;
  const isChecked = prop.isChecked;
  const handleClick = prop.handleClick;
  const index = prop.index;
  const [showModal, setShowModal] = useState(false);
  const [localStorage, setStatus] = useState();
  const dispatch = useDispatch();
  const [soluong, setSoluong] = useState(giohangItem.soluong);
  useEffect(() => {
    setStatus(window.localStorage.getItem("msdn"));
  }, []);
  function handleGiohang(e) {
    const params = {
      msdn: localStorage,
    };
    dispatch(createGiohang.createGiohangRequest(params));
  }
  function soluong_tang(key, e) {
    const soluong_element = document
      .getElementsByClassName("sanpham_giohang")
      [key].getElementsByClassName("input_sl")[0];

    var soluong_input = soluong_element.value;
    var giaban = giohangItem.giaban,
      mshh = giohangItem.mshh,
      dvt = giohangItem.dvt,
      thuesuat = giohangItem.thuesuat;
    var soluong_new = Number(soluong_input) + 1;
    soluong_element.value = soluong_new;
    const handleUpdate_dathangline = async (e) => {
      const thanhtienNew = giaban * parseInt(soluong_new);
      const thanhtienvatNew = thanhtienNew - (thanhtienNew * thuesuat) / 100;

      try {
        const params = {
          msdn: localStorage,
          mshh: mshh,
          soluong: soluong_new,
          dvt: dvt,
          thanhtien: thanhtienNew,
          thanhtienvat: thanhtienvatNew,
        };
        await dathangApi.update_dathangline(params);
        handleGiohang();
        setSoluong(soluong_new);
      } catch (error) {
        console.log(error);
      }
    };
    handleUpdate_dathangline();
  }
  function soluong_giam(key, e) {
    const soluong_element = document
      .getElementsByClassName("sanpham_giohang")
      [key].getElementsByClassName("input_sl")[0];
    var soluong = soluong_element.value;
    var giaban = giohangItem.giaban,
      mshh = giohangItem.mshh,
      dvt = giohangItem.dvt,
      thuesuat = giohangItem.thuesuat;
    if (soluong > 1) {
      var soluong_new = Number(soluong) - 1;
      soluong_element.value = soluong_new;
      const handleUpdate_dathangline = async (e) => {
        const thanhtienNew = giaban * parseInt(soluong_new);
        const thanhtienvatNew = thanhtienNew - (thanhtienNew * thuesuat) / 100;

        try {
          const params = {
            msdn: localStorage,
            mshh: mshh,
            soluong: soluong_new,
            dvt: dvt,
            thanhtien: thanhtienNew,
            thanhtienvat: thanhtienvatNew,
          };
          await dathangApi.update_dathangline(params);
          handleGiohang();
          setSoluong(soluong_new);
        } catch (error) {
          console.log(error);
        }
      };
      handleUpdate_dathangline();
    }
  }
  function open_form_delete(e) {
    e.setShowModal(true);
  }
  function cart_delete(index, e) {
    var msdn = e.msdn,
      rowid = e.rowid;
    const handleDathangDelete = async (e) => {
      try {
        const params = {
          msdn: msdn,
          rowid: rowid,
        };
        await dathangApi.dathangline_delete(params);
        handleGiohang();
        setShowModal(false);
      } catch (error) {
        console.log(error);
      }
    };
    handleDathangDelete();
  }

  return (
    <>
      <tr id="sanpham_giohang" className="sanpham_giohang ">
        <td>
          <div className="flex justify-center">
            <div>
              <div className="form-check">
                <input
                  className="cart_item_check form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                  type="checkbox"
                  value={giohangItem.rowid}
                  id="item_check"
                  name={"item_" + index}
                  onChange={handleClick}
                  defaultChecked={isChecked}
                />
              </div>
            </div>
          </div>
        </td>
        <td className="flex items-center">
          <Image
            className="w-28"
            src={require(`../../../../Backend/upload/sanpham/${giohangItem.path_image}`)}
          />
          <p className="mobile:hidden w-full truncate">{giohangItem.tenhh}</p>
        </td>
        <td className="item_giohang mobile:max-w-[120px] mobile:truncate">
          <p className="fullscreen:hidden tablet:hidden lglap:hidden mobile:w-[70%] mobile:truncate">{giohangItem.tenhh}</p>
          <p>
            {giohangItem.giaban
              .toString()
              .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}{" "}
            đ/{giohangItem.dvt}
          </p>
          <p className="tablet:hidden fullscreen:hidden">
            {giohangItem.thanhtienvat
              .toString()
              .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
          </p>
        </td>
        <td className="item_giohang p-4 mobile:p-1">
          <div className="soluong">
            <button onClick={(e) => soluong_giam(index, e)} className="">
              <Image
                src={require("../../pages/assets/img/icon/minus.png")}
              ></Image>
            </button>
            <input
              onChange={(e) =>
                setSoluong(e.target.value.replace(/[^0-9\.\,]/g, ""))
              }
              className="input_sl w-10 text-[16px] text-center outline-none"
              value={soluong}
            ></input>
            <button onClick={(e) => soluong_tang(index, e)} className="">
              <Image
                src={require("../../pages/assets/img/icon/plus.png")}
              ></Image>
            </button>
          </div>
        </td>
        <td className="item_giohang mobile:hidden">
          {giohangItem.thanhtienvat
            .toString()
            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
        </td>
        <td className="item_giohang" onClick={(e) => setShowModal(true)}>
          <Image src={require("../../pages/assets/img/icon/trash.png")} />
        </td>
      </tr>
      {showModal ? (
        <div className="bg-[#000000b5] justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none">
          <div className="modal-dialog relative w-auto pointer-events-none">
            <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
              <div className="modal-body relative p-4">
                <div className="px-6 text-center">
                  <svg
                    aria-hidden="true"
                    className="mx-auto mb-4 text-gray-400 w-14 h-14 dark:text-gray-200"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                    ></path>
                  </svg>
                  <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                    Bạn muốn sản phẩm {giohangItem.tenhh}?
                  </h3>
                  <button
                    onClick={(e) => cart_delete(index, giohangItem)}
                    data-modal-hide="popup-modal"
                    type="button"
                    className="text-white bg-green-500 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2"
                  >
                    Đồng ý
                  </button>
                  <button
                    onClick={() => setShowModal(false)}
                    data-modal-hide="popup-modal"
                    type="button"
                    className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                  >
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default GioHangItem;
